   <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
				<div class="image">
                    <img src="images/munajat.jpg" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo sesi('namalengkap');?></div>
                    <div class="email"><?php echo sesi('mailuser');?></div>

                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href='#EditInfo' data-toggle='modal' data-id='2'><i class="material-icons">info</i>Info</a></li>
                            <li><a href='#EditProfile'   data-toggle='modal' data-id='<?php user(0);?>'><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="logout.php"><i class="material-icons">input</i>Keluar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list" id="main-navigation">      
				<li class="header">
				<div style="font-size:12px;padding-left:5px">
		<span id="Date"></span><span id="clock"></span>
			</div>
			</li>
                    <li class="">
                        <a href="?module=home">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
					<li class="header">FOLDER</li>
                    <li>
                        <a href="?module=inbox">
                            <i class="material-icons">inbox</i>
                            <span>SMS Masuk</span>
                        </a>
                    </li>
					<li>
                        <a href="?module=outbox">
                            <i class="material-icons">move_to_inbox</i>
                            <span>SMS Keluar</span>
                        </a>
                    </li>
					<li>
                        <a href="?module=send">
                            <i class="material-icons">send</i>
                            <span>SMS Terkirim</span>
                        </a>
                    </li>
					<li>
                        <a href="?module=email">
                            <i class="material-icons">mail</i>
                            <span>Email Terkirim</span>
                        </a>
                    </li>
					<li>
                        <a href="?module=schedule">
                            <i class="material-icons">schedule</i>
                            <span>SMS Terjadwal</span>
                        </a>
                    </li>
                   
                    <li class="header">PENGATURAN</li>
                    <li class="active">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">contact_phone</i>
                            <span>Kontak</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="?module=kontak">List Kontak</a>
                            </li>
                            <li>
                                <a href="?module=group">Grup Kontak</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="?module=laporan">
                            <i class="material-icons">print</i>
                            <span>Laporan</span>
                        </a>
                    </li>
                    <li>
                        <a href="?module=autoreplay">
                            <i class="material-icons">replay</i>
                            <span>Auto Replay</span>
                        </a>
                    </li>
					<li>
                        <a href="?module=themes">
                            <i class="material-icons">extension</i>
                            <span>Template SMS</span>
                        </a>
                    </li>
					<li>
                        <a href="services/"  id="">
                            <i class="material-icons">settings</i>
                            <span>Pengaturan Modem</span>
                        </a>
                    </li>
                    <li>
                        <a href="?module=database">
                            <i class="material-icons">cloud</i>
                            <span>Database</span>
                        </a>
                    </li>
					<li>
                        <a href="?module=changelogs">
                            <i class="material-icons">update</i>
                            <span>Changelogs</span>
                        </a>
                    </li>
					<li>
                        <a href="logout.php">
                            <i class="material-icons">power_settings_new</i>
                            <span>Keluar</span>
                        </a>
                    </li>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
<?php 
$sqlv = $mysqli->query("SELECT * FROM changelogs order by id desc limit 1");
$datav=$sqlv->fetch_array();
$copyYear = 2016; 
$curYear = date('Y'); 
echo '&#169;&#160;' . $copyYear . (($copyYear != $curYear) ? ' - ' . $curYear : ''); 
?> <a href="javascript:void(0);"><?php info('info',1,2);?></a> <b>Versi: </b> <a href="?module=changelogs"><?=$datav['judul'];?></a>
                </div>
                <div class="version">
                   Style by <a href="https://gurayyarar.github.io/AdminBSBMaterialDesign/" target="_blank">AdminBSB</a> <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" ><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation" class="active"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade " id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade in active in active" id="settings">
                    <div class="demo-settings">
                        <p>PENGATURAN MODEM</p>
<div class="panel-group full-body" id="accordion_5" role="tablist" aria-multiselectable="true">
<?php
$maxmodem = 8;
$sum = 0;
$drive = driveLetter(__FILE__);
for($i=1; $i<=$maxmodem; $i++)
{
	if (is_file($drive.':\xampp\htdocs\smsku\services\smsdrc'.$i))
	{
		$sum = $i + 1;
	}	
}

if ($sum == 0) $sum = 1;

$nextsmsdrc = $drive.":\xampp\htdocs\smsku\services\smsdrc".$sum;
$sum = 0;
for($i=1; $i<=$maxmodem; $i++)
{
	if (is_file($drive.':\xampp\htdocs\smsku\services\smsdrc'.$i))
	{
		$sum++;
	}	
}
if ($sum == 0) echo "<ul class='setting-list'><li><span>Phone/Modem belum ada</span></li></ul>";
else{
$count = 0;
for($i=1; $i<=$maxmodem; $i++)
{
	if (is_file($drive.':\xampp\htdocs\smsku\services\smsdrc'.$i))
	{
			$count++;
			$handle = @fopen("services\smsdrc".$i, "r");
			if ($handle) 
			{
				while (!feof($handle)) 
				{
					$buffer = fgets($handle);
					if (substr_count($buffer, 'port = ') > 0)
					{
						$split = explode("port = ", $buffer);
						$port = $split[1];
					}
					if (substr_count($buffer, 'phoneid = ') > 0)
					{
						$split = explode("phoneid = ", $buffer);
						$phone = $split[1];
					}
					if (substr_count($buffer, 'connection = ') > 0)
					{
						$split = explode("connection = ", $buffer);
						$conn = $split[1];
					}
				}
			}
?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne_<?=$i;?>">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_5" href="#collapseOne_<?=$i;?>" aria-expanded="false" aria-controls="collapseOne_<?=$i;?>">
                                                       <?=$phone;?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_<?=$i;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_<?=$i;?>">
                                                <div class="panel-body">
												<ul class="setting-list">
													<li>
														<span>Nama Modem</span>
														<div class="switch"><label>
														<strong><?=$phone;?></strong>
														</label></div>
													</li>
													<li>
														<span>Port Modem</span>
														<div class="switch"><label>
														<strong><?=$port;?></strong>
														</label></div>
													</li>
													<li>
														<span>Connection Modem</span>
														<div class="switch"><label>
														<strong><?=$conn;?></strong>
														</label></div>
													</li>
													<li>
														<span>Cek Modem</span>
														<div class="switch"><label>
														<a href="#" data-toggle="modal" id="<?=$i;?>" class="cek" data-target="#cek-koneksi" data-backdrop="static" data-keyboard="false"><i class="material-icons col-red" data-toggle="tooltip" data-placement="left" title="Cek Koneksi modem <?=$phone;?>">network_check</i></a>
														</label></div>
													</li>
													<li>
													<span>Install & Jalankan Service</span>
													<div class="switch"><label>
													<a href="#" data-toggle="modal" id="<?=$i;?>" class="plays" data-target="#play-koneksi" data-backdrop="static" data-keyboard="false"><i class="material-icons col-red" data-toggle="tooltip" data-placement="left" title="Started Service modem <?=$phone;?>" data-backdrop="static" data-keyboard="false">play_arrow</i></a>
													</label> </div>
													</li>
													<li>
													<span>Matikan Modem</span>
													<div class="switch"><label>
													<a href="#" data-toggle="modal" id="<?=$i;?>" class="stop" data-target="#stop-koneksi" data-backdrop="static" data-keyboard="false"><i class="material-icons col-red" data-toggle="tooltip" data-placement="left" title="Stop Service GammuSMSD" data-backdrop="static" data-keyboard="false">stop</i></a>
													</label></div>
													</li>
													<li>
												</ul>
                                                </div>
                                            </div>
                                        </div>
<?php
		$sum++;
		
	}
}
}
?>
						</div>
<p>BACKUP DATABASE</p>
<ul class="setting-list">
<li>
<span>Klik untuk backup</span>
<div class="switch"><label>
<a href="#" data-toggle="modal" id="1" class="backup" data-target="#backup" data-backdrop="static" data-keyboard="false"><i class="material-icons col-red" data-toggle="tooltip" data-placement="left" title="Backup Database">backup</i></a>
</label></div>
</li>
</ul>
                    </div>
                </div>
				
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>
            