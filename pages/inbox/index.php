﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
header('location:../index.php');
}else{
switch($act){
  // Tampil List berita
  default:
	if ($status == "read") {
        $q_cek_inbox = mysqli_query($mysqli,"update inbox set Processed='true' where ID='$GETID'");
    }
?>
  <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA SMS MASUK
                            </h2>
							 <ul class="header-dropdown m-r--5 m-t--10">
								<li>
								<input type="checkbox" id="bulkDelete" class="chk-col-red"/>
													<label for="bulkDelete">PILIH SEMUA </label></li>
                               <li><button id="deleteTriger" class="btn btn-danger"><i class="material-icons ">delete</i> HAPUS SMS</button>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="lib/print_pdf_inbox.php" class="btn bg-green" target="_blank"><i class="material-icons col-red">picture_as_pdf</i>SMS HARI INI</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                        <div class="body table-responsive">
                            <table id="jsontable" class="table table-bordered table-striped table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th style="width:1% !important;" >No</th>
												<th  class="no-sort"style="width:1%;text-align:center">
												 Pilih</th>
                                                <th style="width:16%;">Pengirim</th>
                                                <th>Text</th>
                                                <th style="width:16%;text-align:center">Tanggal</th>
                                                <th class="no-sort" style="width:20%;text-align:center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
        </div>
            <!-- replay modal -->
<div class="modal fade" id="ReplayModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Balas SMS</h4>
            </div>
            <div class="modal-body">
                <div class="fetched-data"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="TambahModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <div class="modal-body">
                <div id="loadrep"></div>
                <div class="tambah-data"></div>
            </div>
        </div>
    </div>
</div>
<!-- baca modal -->
<div class="modal fade" id="BacaModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Baca Pesan</h4>
            </div>
            <div class="modal-body">
                <div id="loadbaca"></div>
                <div class="baca-data"></div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-link waves-effect" data-dismiss="modal">TUTUP</button>
			</div>
        </div>
    </div>
</div>
            <!-- For Material Design Colors -->
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content modal-col-red">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body">
						<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
						<p>Apakah Anda ingin melanjutkan?</p>
						<!--p class="debug-url"></p-->
						<input type="hidden" id="dell-url" value="">
                        </div>
                        <div class="modal-footer">
						    <button type="button" class="btn btn-danger" id="btnDelteYes" href="#">Yes</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                        </div>
                    </div>
                </div>
            </div>
			<!-- For Material Design Colors -->
            <div class="modal fade" id="confirm-deleteA" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content modal-col-red">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body">
						<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
						<p>Apakah Anda ingin melanjutkan?</p>
						<!--p class="debug-url"></p-->
						<input type="hidden" id="dell-urla" value="">
                        </div>
                        <div class="modal-footer">
						    <button type="button" class="btn btn-danger" id="btnDelteAll" href="#">Yes</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                        </div>
                    </div>
                </div>
            </div>
			<!-- For Material Design Colors -->
            <div class="modal fade" id="confirm-alert" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content modal-col-red">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Delete Confirm Alert</h4>
                        </div>
                        <div class="modal-body">
						<p>Data belum dipilih.</p>
						<p>Silahkan dipilih data yang akan dihapus.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                        </div>
                    </div>
                </div>
            </div>
			
<?php
    break;
	}
}
 ?>