﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
switch($act){
  // Tampil List berita
  default:

?>
  <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA KONTAK
                            </h2>
                            <ul class="header-dropdown m-r--5 m-t--10">
                                <li>
                                    <a href="#" class="btn bg-green" onClick="showModalsk()">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Tambah kontak">add</i>
                                    </a>
                                </li>
								<li>
                                    <a href="#" data-toggle="modal" id="0" class="tambah btn bg-green" data-target="#import-md">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Import kontak">import_contacts</i>
                                    </a>
                                </li>
								<li>
                                    <a href="lib/export.php" class="btn bg-green" target="_blank">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Export kontak">import_export</i>
                                    </a>
                                </li>
								<li>
                                    <a href="lib/print_pdf.php" class="btn bg-green" target="_blank">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Cetak PDF">picture_as_pdf</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table id="js-kontak" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:1% !important;" >No</th>
                                                <th style="width:15%;">Nama</th>
                                                <th style="width:10%;">Group</th>
                                                <th style="width:10%;">Nomor</th>
                                                <th style="width:10%;text-align:center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="card">
                        <div class="header">
                            <h2>
                                GRUP KONTAK
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table id="js-groups" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Grup</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										</tbody>
                            </table>
                        </div>
                    </div>
                 </div>
            </div>
			
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
        </div>
            <div class="modal fade" id="dialog-pb" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabelg">Group Phonebook</h4>
                        </div>
                        <div class="modal-body">
						<div class="pb-grup"></div>
                        </div>
                    </div>
                </div>
            </div>
		<!-- Modal add -->
		<div class="modal fade" id="myModalsK" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add Data</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger" role="alert" id="removeWarning">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Anda yakin ingin menghapus data ini
						</div>
						<br>
						<form id="formUserK">
							<input type="hidden" class="form-control" id="id" name="id">
							<input type="hidden" class="form-control" id="type" name="type">
				<div class="body">
					<div class="row clearfix">
						<div class="col-md-12">
							<b>Nama <span class="errorn" style="display:none;color:#ff0000;"></span></b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">contact_phone</i>
								</span>
								<div class="form-line">
					<input type="text" id="nama" name="nama" class="form-control clearable" placeholder="Nama" required autofocus />
								</div>
							</div>
							<b>Email</b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">mail</i>
								</span>
								<div class="form-line">
					<input type="email" id="emails" name="emails" class="form-control clearable" placeholder="email" required />
								</div>
							</div>
					<label for="negara">Negara <span class="errorneg" style="display:none;color:#ff0000;"></span></label>
				<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">flag</i>
								</span>
					<div class="form-line">
                <select name="negara" id="negara" onchange="myFunctions()" class="form-control">
                    <option value="">-- Pilih Negara --</option>
                    <?php
					$sqn = "select * from negara";
					$rsn = $mysqli->query($sqn);
                    while ($an = $rsn->fetch_object()){ ?>
                          <option value="<?=$an->Kode;?>"><?=$an->Nama;?></option>
                    <?php }
                    ?>
                </select>
					</div>
				</div>
							<label for="kode">Kode Negara & Nomor HP <span class="errorno" style="display:none;color:#ff0000;"></span></label>
                              <div class="row">
                              <div class="col-md-3">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">flag</i>
								</span>
							  <div class="form-line">
						<input type="text" id="kode" name="kode" class="form-control" placeholder="Kode" readonly="readonly" />
                                </div>
                                </div>
                                </div>
								 <div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">phone_iphone</i>
								</span>
								 <div class="form-line">
					<input type="text" id="nomor" name="nomor" class="form-control mobile-phone-number clearable" autocomplete="off" placeholder="Nomor HP" required />
								</div>
								</div>
							</div>
							</div>
					<label for="groupid" class="hidex">Group <span class="errort" style="display:none;color:#ff0000;"></span></label>
				<div class="input-group hidex">
								<span class="input-group-addon">
									<i class="material-icons">folder</i>
								</span>
					<div class="form-line">
                <select name="groupid" id="groupid" class="form-control custom">
                    <option value="">-- Pilih Group --</option>
                    <?php
					$sq = "select * from pbk_groups";
					$rs = $mysqli->query($sq);
                    while ($a = $rs->fetch_object()){ ?>
                          <option value="<?=$a->ID;?>"><?=$a->Name;?></option>
                    <?php }
                    ?>
                </select>
					</div>
				</div>
						</div>
					</div>
				</div>
						</form>
						
					</div>
					<div class="modal-footer">
						<button type="button" onClick="submitUserk()" id="kirimB" class="btn btn-success">Submit</button>
						<button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
<div class="modal fade" id="import-md" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Import Kontak</h4>
            </div>
                <div class="modal-body">
                    <form action="?module=kontak&act=import" method="POST" enctype="multipart/form-data">
			<div class="body">
					<div class="row clearfix">
						<div class="col-md-12">
				<div class="form-group">
					<div class="form-line">
					<input type="file" name="uploadno">
					</div>
				<code>Format xls</code>
				</div>
						</div>

					</div>
			</div>
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-default">Upload</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">TUTUP</button>
                </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
		<!-- For Material Design Colors -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content modal-col-red">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
					</div>
					<div class="modal-body">
					<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
					<p>Apakah Anda ingin melanjutkan?</p>
					</div>
					<div class="modal-footer">
					   <a href="#" class="btn btn-danger danger">HAPUS</a>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
					</div>
				</div>
			</div>
		</div>

<?php
    break;
	case "import":
	include __DIR__ . '/excel_reader.php';
	include __DIR__ . '/import.php';
    break;

	}
}
 ?>