<?php
// error_reporting(0);
//Include DB configuration file
include('../../g-asset/conn_db.php');
include('../../g-asset/web_function.php');

//sanitize post value
$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

//throw HTTP error if page number is not valid
if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit();
}

//get current starting point of records
$position = (($page_number-1) * $item_per_page);

//fetch records using page position and item per page. 
$results = $mysqli->prepare("SELECT id,judul,isi,tanggal,gambar FROM changelogs ORDER BY id DESC LIMIT ?, ?");

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
//for more info https://www.sanwebe.com/2013/03/basic-php-mysqli-usage
$results->bind_param("dd", $position, $item_per_page); 
$results->execute(); //Execute prepared Query
$results->bind_result($id, $judul, $isi, $tanggal,$gambar); //bind variables to prepared statement

//output results from database
while($results->fetch()){ //fetch values
?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?php echo $judul; ?>
                                <small><?php echo dtimes($tanggal,true,true); ?></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="#" data-toggle="modal" id="<?=$id;?>" class="edit" data-target="#dialog-ch"><i class="material-icons col-red">edit</i> Edit</a></li>
                                        <li><a href="#" data-record-id="<?=$id;?>" data-toggle="modal" data-target="#confirm-delete"><i class="material-icons col-red">delete</i> Hapus</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                        <div class="row">
						<div class="col-md-10">
                            <?php echo nl2br($isi); ?>
						</div>
						<div class="col-md-2">
						<?php 
						if($gambar==""){ ?>
                                   <a href="#imagemodal" class="pops" data-title="<?php echo $judul; ?>">
                                        <img class="img-responsive thumbnail" src="images/no_image.png">
                                    </a>
						<?php }else{
							?>
                                    <a href="#imagemodal" class="pop" data-title="<?php echo $judul; ?>">
                                        <img class="img-responsive thumbnail" src="images/uploads/<?php echo $gambar; ?>">
                                    </a>
						<?php } ?>
                        </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
<?php } ?>
<script>
        var id_ch = 0;
        $('.edit,.tambah').on("click", function(){
            var url = "pages/changelogs/edit.form.php";
            id_ch = this.id;
            if(id_ch != 0) {
                $("#myModalLabelc").html("Update Data Changelogs");
            } else {
                $("#myModalLabelc").html("Tambah Data changelogs");
            }
            $.post(url, {id: id_ch} ,function(data) {
                $(".pb-edit").html(data).show();
            });
        });
		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
			var title = $(this).data('title');
			$("#myModalLabelM").html(title);
		});	
</script>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content">  
	<div class="modal-header">
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabelM">Title</h4>
	</div>
      <div class="modal-body">
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>  
    </div>
  </div>
</div>