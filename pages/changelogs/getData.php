
<?php
if(isset($_POST["id"]) && !empty($_POST["id"])){

//Include DB configuration file
include('../../g-asset/conn_db.php');

//Get last ID
$lastID = $_POST['id'];

//Limit on data display
$showLimit = 1;

//Get all rows except already displayed
$queryAll = $mysqli->query("SELECT COUNT(*) as num_rows FROM changelogs WHERE id < ".$lastID." AND publish='Y' ORDER BY id DESC");
$rowAll = $queryAll->fetch_assoc();
$allNumRows = $rowAll['num_rows'];

//Get rows by limit except already displayed
$query = $mysqli->query("SELECT * FROM changelogs WHERE id < ".$lastID." AND publish='Y' ORDER BY id DESC LIMIT ".$showLimit);

if($query->num_rows > 0){
    while($row = $query->fetch_assoc()){
        $postID = $row["id"]; ?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?php echo $row['judul']; ?>
                                <small><?php echo $row['tanggal']; ?></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="#" data-toggle="modal" id="<?=$postID;?>" class="edit" data-target="#dialog-ch"><i class="material-icons col-red">edit</i> Edit</a></li>
                                         <li><a href="#" data-record-id="<?=$row['id'];?>" data-toggle="modal" data-target="#confirm-delete"><i class="material-icons col-red">delete</i> Hapus</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                                                    <div class="row">
						<div class="col-md-10">
                            <?php echo nl2br($row['isi']); ?>
						</div>
						<div class="col-md-2">
						<?php 
						if($row['gambar']==""){ ?>
                                   <a href="#" class="pop" data-sub-html="<?php echo $row['judul']; ?>">
                                        <img class="img-responsive thumbnail" src="images/no_image.png">
                                    </a>
						<?php }else{
							?>
                                    <a href="#" class="pop" data-sub-html="<?php echo $row['judul']; ?>">
                                        <img class="img-responsive thumbnail" src="images/uploads/<?php echo $row['gambar']; ?>">
                                    </a>
						<?php } ?>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

<?php } ?>
<script>
        var id_ch = 0;
        $('.edit,.tambah').on("click", function(){
            var url = "pages/changelogs/edit.form.php";
            id_ch = this.id;
            if(id_ch != 0) {
                $("#myModalLabelc").html("Update Data Changelogs");
            } else {
                $("#myModalLabelc").html("Tambah Data changelogs");
            }
            $.post(url, {id: id_ch} ,function(data) {
                $(".pb-edit").html(data).show();
            });
        });
		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});	
</script>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content"  >              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div> 
      <div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>   
    </div>
  </div>
</div>
<?php if($allNumRows > $showLimit){ ?>
    <div class="load-more" lastID="<?php echo $postID; ?>" style="display: none;">
        <img src="images/ajax-loader.gif"/>
    </div>
<?php }else{ ?>
    <div class="load-more" lastID="0">
                            <div class="alert alert-success">
                                <strong>Well done!</strong> No more data.
                            </div>
    </div>
<?php }
    }else{ ?>
    <div class="load-more" lastID="0">
                            <div class="alert alert-success">
                                <strong>Well done!</strong> No more data.
                            </div>
    </div>
<?php
    }
}
?>