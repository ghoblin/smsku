﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
switch($act){
  // Tampil List berita
  default:
?>
<style>
.modal.loading .modal-content:before {
  content: 'Loading...';
  text-align: center;
  line-height: 155px;
  font-size: 20px;
  background: rgba(0, 0, 0, .8);
  position: absolute;
  top: 55px;
  bottom: 0;
  left: 0;
  right: 0;
  color: #EEE;
  z-index: 1000;
}
img {
    display: block;
    margin: 0 auto;
}
</style>
<div class="container-fluid">
            <!-- Changelogs -->
            <div class="block-header">
            <div class="pull-right"><a href="#" class="tambah" id="0" data-toggle="modal" data-target="#dialog-ch"><i class="material-icons col-red">add</i></a>
            </div>
                <h2>CHANGELOGS</h2>
            </div>
			
    <div class="row">
        <div class="col-lg-12">
		<div id="aniimated-thumbnials" class="list-unstyled row clearfix">
		<div class="post-list" id="results">
		<div class="loading-info alert alert-success"><img src="images/ajax-loader.gif"/></div>
        </div>
        </div>
        </div>
    </div>
	
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content"  >              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div> 
      <div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
          
          
    </div>
  </div>
</div>
            <div class="modal fade" id="dialog-ch" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabelc">Edit Changelogs</h4>
                        </div>
                        <div class="modal-body">
						<div class="pb-edit"></div>
                        </div>
                    </div>
                </div>
            </div>
    <div class="modal fade" id="confirm-deletex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p>You are about to delete <b><i class="title"></i></b> record, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger btn-ok">Delete</button>
                </div>
            </div>
        </div>
    </div>
            <!-- For Material Design Colors -->
            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content modal-col-red">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body">
						<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
						<p>Apakah Anda ingin melanjutkan?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-ok">Delete</button>
                            <button type="button" class="btn btn-link waves-effect btn-ok" data-dismiss="modal">TUTUP</button>
                        </div>
                    </div>
                </div>
            </div>
</div>

<?php
    break;
	}
}
 ?>
