<?php
// error_reporting(0);
session_start();
if (empty($_SESSION['namauser'])){
echo json_encode(array(404 => "error"));
}else{
	include "../../g-asset/conn_db.php";
    $id_pbk = $_POST['id'];

	$sql = $mysqli->query("SELECT * FROM changelogs WHERE id='$id_pbk'");
	$data=$sql->fetch_array();
    if($id_pbk > 0) {
        $id_pbk = $data['id'];
        $judul = $data['judul'];
        $isi = $data['isi'];
        $tanggal = $data['tanggal'];
        $gambar  = "images/uploads/".$data['gambar'];
        $ket= "update";

    } else  {
        $id_pbk = "";
        $judul = "";
        $isi  = "";
        $tanggal  = "";
        $gambar  = "";
        $ket= "add";
    }

    ?>
    <!--form id="edit_data" method="post" enctype="multipart/form-data" --> 
	<form action="pages/changelogs/processupload.php" method="post" enctype="multipart/form-data" id="MyUploadForm">
	<input type="hidden" class="form-control" id="type" name="type" value="<?=$ket;?>">
	<div class="demo-masked-input">
	<div id="output"></div>
	<div class="error"></div>
        <div class="form-group" hidden="hidden">
            <label class="col-sm-2 col-sm-2 control-label">ID</label>
            <div class="col-sm-2">
                <input type="text" name="idpbk" id="idpbk" value="<?php echo $id_pbk ?>">
                <input type="text" name="postList" id="postList" value="postList">
            </div>
        </div>
		<div class="row clearfix">
			<div class="col-md-3 form-control-label">
				<label for="">Versi</label>
			</div>
			<div class="col-md-9">
				<div class="form-group form-float">
					<div class="form-line">
						<input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul ?>" required>
					</div>
					<span class="error-judul" style="display:none;color:#ff0000;"></span>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-3 form-control-label">
				<label for="">Deskripsi</label>
			</div>
			<div class="col-md-9">
					<div class="input-group">
					<div class="form-line">
						<textarea rows="2" id="isi" name="isi" class="form-control no-resize auto-growth" placeholder="Deskripsi" required><?php echo $isi ?></textarea>
					</div><span class="error-isi" style="display:none;color:#ff0000;"></span>
				   </div>
			</div>
		</div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label for="">Upload Gambar</label>
				<div class="form-group">
                  <input type="file" id="FileInput" name='FileInput' class="dropify" data-default-file="<?=$gambar;?>"/>
				</div>
                </div>
            </div>
		<div class="row clearfix">
			<div class="col-md-3 form-control-label">
				<label for="">Tanggal</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" id="tanggal" name="tanggal" class="tanggal form-control" placeholder="Pilih tanggal..." value="<?php echo $tanggal; ?>" required>
					</div>
					<span class="error-judul" style="display:none;color:#ff0000;"></span>
				</div>
			</div>
		</div>
	<div class="modal-footer">
		<button type="submit" name="edit" value="edit" id="submit-btn" class="btn  btn-success waves-effect"><?php echo $ket?></button>
		<button type="button" class="btn btn-link bg-red waves-effect" data-dismiss="modal">CLOSE</button>
	</div>
	</div>
    </form>
	<div id="progressbox" ><div id="progressbar"></div ><div id="statustxt">0%</div></div>
<script type="text/javascript">
$(document).ready(function() { 
$('.dropify').dropify();
	var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			success:       afterSuccess,  // post-submit callback 
			uploadProgress: OnProgress, //upload progress callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
	 $('#MyUploadForm').submit(function() { 
			$(this).ajaxSubmit(options);  			
			// always return false to prevent standard browser submit and page navigation 
			return false; 
		}); 
		

//function after succesful file upload (when server response)
function afterSuccess()
{
	var type = $("#type").val();
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button
	$('#progressbox').delay( 1000 ).fadeOut(); //hide progress bar
	  $('#dialog-ch').modal('hide');
	   delnotif('white','Data berhasil di '+type,type);
	  setTimeout(function(){window.location.reload(1);}, 1000);
}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		
		if(!$('#FileInput').val()) //check empty input filed
		{
			$("#output").html("File Kosong?");
			return true
		}
		if( !$('#isi').val()) //check empty input filed
		{
			$("#output").html("Isi Kosong?");
			return false
		}
		// if( !$('#isi').val()) //check empty input filed
		// {
			// $("#tanggal").html("Tanggal Kosong?");
			// return false
		// }
		
		var fsize = $('#FileInput')[0].files[0].size; //get file size
		var ftype = $('#FileInput')[0].files[0].type; // get file type
		

		//allow file types 
		switch(ftype)
        {
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html':
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		
		//Allowed file size is less than 5 MB (1048576)
		if(fsize>5242880) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big file! <br />File is too big, it should be less than 5 MB.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older unsupported browsers that doesn't support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//progress bar function
function OnProgress(event, position, total, percentComplete)
{
    //Progress bar

	$('#progressbox').show();
    $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
    $('#statustxt').html(percentComplete + '%'); //update status text
    if(percentComplete>50)
        {
            $('#statustxt').css('color','#000'); //change status text to white after 50%
        }
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

}); 

</script>
<script>
// $(document).ready(function() {
	// $('.dropify').dropify();
     // $('#edit_data').on('submit', function(event){
           // event.preventDefault();  
           // if($('#judul').val() == '')  
           // {  
               // $('.error-judul').html('Judul masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
           // }  
           // else if($('#isi').val() == '')  
           // {  
               // $('.error-isi').html('Deskripsi masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
           // }   
           // else  
           // {  
                // $.ajax({
                     // url:'pages/changelogs/save.php',  
                     // method:'POST',  
                     // data:$('#edit_data').serialize(),  
                     // beforeSend:function(){  
                          // $('#edit').val('Inserting');  
                     // },  
                     // success:function(data){
						 // if(data=="add"){
                          // $('#edit_data')[0].reset();  
                          // $('#dialog-ch').modal('hide');
						   // delnotif('white','Data berhasil di tambah','add');
						  // setTimeout(function(){window.location.reload(1);}, 1000);
						 // }else if(data=="upp"){
                          // $('#edit_data')[0].reset();  
                          // $('#dialog-ch').modal('hide');
						   // delnotif('white','Data berhasil di update','update');
						  // setTimeout(function(){window.location.reload(1);}, 1000);
						 // }else{
							 // $(".error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+data+' !</div>');
						 // }
                     // }  
                // });  
           // }  
      // });
      // });
$(function () {
    //Datetimepicker plugin
    $('.tanggal').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD HH:mm',
        clearButton: true,
        weekStart: 1
    });


});
</script>

<?php } ?>