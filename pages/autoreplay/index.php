﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
switch($act){
  // Tampil List berita
  default:

?>
  <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                KEYWORD SMS AUTOREPLAY
                            </h2>
                            <ul class="header-dropdown m-r--5 m-t--10">
                                <li>
                                    <a href="#" class="btn bg-green" onClick="showModalsR()">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Tambah kontak">add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table id="js-auto" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:1% !important;" >No</th>
                                                <th style="width:15%;">KEYWORD</th>
                                                <th>KET</th>
                                                <th style="width:10%;text-align:center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
		    </div>
			
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
        </div>
            <div class="modal fade" id="dialog-pb" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabelg">Group Phonebook</h4>
                        </div>
                        <div class="modal-body">
						<div class="pb-grup"></div>
                        </div>
                    </div>
                </div>
            </div>
		<!-- Modal add -->
		<div class="modal fade" id="myModalsR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add Data</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger" role="alert" id="removeWarning">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Anda yakin ingin menghapus data ini
						</div>
						<br>
						<form id="formUserR">
							<input type="hidden" class="form-control" id="id" name="id">
							<input type="hidden" class="form-control" id="type" name="type">
				<div class="body">
					<div class="row clearfix">
						<div class="col-md-12">
							<b>FORMAT KEYWORD <span class="error1" style="display:none;color:#ff0000;"></span></b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">note</i>
								</span>
								<div class="form-line">
								<input type="text" id="key1" name="key1" class="form-control clearable" autofocus />
								</div>
							</div>
							<b>ISI KEYWORD <span class="error2" style="display:none;color:#ff0000;"></span></b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">note_add</i>
								</span>
								<div class="form-line">
								<input type="text" id="key2" name="key2" class="form-control clearable" />
								</div>
							</div>
						</div>
					</div>
				</div>
						</form>
						
					</div>
					<div class="modal-footer">
						<button type="button" onClick="submitUserR()" class="btn btn-default">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
<div class="modal fade" id="import-md" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Import Kontak</h4>
            </div>
                <div class="modal-body">
                    <form action="?module=kontak&act=import" method="POST" enctype="multipart/form-data">
			<div class="body">
					<div class="row clearfix">
						<div class="col-md-12">
				<div class="form-group">
					<div class="form-line">
					<input type="file" name="uploadno">
					</div>
				<code>Format xls</code>
				</div>
						</div>

					</div>
			</div>
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-default">Upload</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">TUTUP</button>
                </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
		<!-- For Material Design Colors -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content modal-col-red">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
					</div>
					<div class="modal-body">
					<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
					<p>Apakah Anda ingin melanjutkan?</p>
					</div>
					<div class="modal-footer">
					   <a href="#" class="btn btn-danger danger">HAPUS</a>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
					</div>
				</div>
			</div>
		</div>

<?php
    break;
	case "save":
	include __DIR__ . '/save.php';
    break;	
	case "import":
	include __DIR__ . '/excel_reader.php';
	include __DIR__ . '/import.php';
    break;
	case "hapus":
	//hapus data
	$del = $mysqli->prepare("DELETE FROM pbk WHERE ID=?");
	$del->bind_param("i",$GETID);
	if($del->execute())
	{
	save_alert('delete',delete);
	htmlRedirect('?'.$mode.'='.$module.'&alert=delete');	
	}else{
	save_alert('error',$insert->error);
	}
    break;
    break;
	}
}
 ?>