﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
switch($act){
  // Tampil List berita
  default:

?>
  <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA PESAN SCHEDULER
                            </h2>
                            <ul class="header-dropdown m-r--5 m-t--10">
								<li>
                                    <button onClick="showModals()" class="btn btn-success"><i class="material-icons ">add</i> SCHEDULE SMS </button>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table id="js-sch" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:1% !important;" >NO</th>
                                                <th>NO. TUJUAN</th>
                                                <th>ISI SMS</th>
                                                <th>TANGGAL</th>
                                                <th style="width:10%;text-align:center">AKSI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
        </div>
		<!-- Modal add -->
		<div class="modal fade" id="myModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add Data</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger" role="alert" id="removeWarning">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Anda yakin ingin menghapus data ini
						</div>
						<br>
						<form id="formUser">
							<input type="hidden" class="form-control" id="id" name="id">
							<input type="hidden" class="form-control" id="type" name="type">
				<div class="body">
				<div class="demo-masked-input">
					<div class="row clearfix">
						<div class="col-md-12">
							<b>Nomor HP <span class="errorn" style="display:none;color:#ff0000;"></span></b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">phone_iphone</i>
								</span>

					<div class="search-box">
					<input type="text" id="nomorhp" name="nomorhp" class="form-control mobile-phone-number clearable" autocomplete="off" placeholder="Cari Nomor..." autofocus />
					<div class="result"></div>
					</div>
							</div>
					<label>Pesan <span class="errorp" style="display:none;color:#ff0000;"></span></label>
					<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">message</i>
								</span>
					<div class="form-line">
						<textarea rows="2" id="pesansc" name="pesansc" class="form-control no-resize auto-growth clearable" placeholder="Tulis pesan" maxlength="153" onkeyup="countChar(this)" required></textarea>
					</div>
					<div id="charNum"></div>
					</div>
					<label>Tanggal <span class="errort" style="display:none;color:#ff0000;"></span></label>
					<div class="input-group">
					<span class="input-group-addon">
					<i class="material-icons">perm_contact_calendar</i>
					</span>
					<div class="form-line">
						<input type="text" id="time" name="time" class="datetimepicker form-control" placeholder="Pilih tanggal..." required="required">
					</div>
                    </div>
						</div>
					</div>
				</div>
				</div>
						</form>
						
					</div>
					<div class="modal-footer">
						<button type="submit" onClick="submitUser()" class="btn btn-default">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- For Material Design Colors -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content modal-col-red">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
					</div>
					<div class="modal-body">
					<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
					<p>Apakah Anda ingin melanjutkan?</p>
					</div>
					<div class="modal-footer">
					   <a href="#" class="btn btn-danger danger">HAPUS</a>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
					</div>
				</div>
			</div>
		</div>

<?php
    break;
	case "tambah":
	$pathFolder = "pages/".$module."/form.php";
	include $pathFolder;
    break;
	case "edit":
	$pathFolder = "pages/".$module."/form.php";
	include $pathFolder;
    break;
	case "save":
	$pathFolder = "pages/".$module."/save.php";
	include $pathFolder;
    break;
	case "hapus":
	// //cari gambar	
	// $res = $mysqli->query("SELECT folder,gambar FROM posting WHERE id_post=".$GETID);
	// $r = $res->fetch_array();
	// $tahun = folderthn($r['folder']);
	// $bulan = folderbln($r['folder']);
	// $gambar1 = "images/post/".$tahun."/".$bulan."/$r[gambar]";
	// $gambar2 = "images/post/".$tahun."/".$bulan."/thumb_$r[gambar]";
	// $gambar3 = "images/post/".$tahun."/".$bulan."/200x200_$r[gambar]";
	// if (file_exists($gambar2) AND $r['gambar'] != 'default.jpg'){
	  // unlink($gambar1);
	  // unlink($gambar2);
	  // unlink($gambar3);
   // }
	//hapus data
	$del = $mysqli->prepare("DELETE FROM pbk WHERE ID=?");
	$del->bind_param("i",$GETID);
	if($del->execute())
	{
	save_alert('delete',delete);
	htmlRedirect('?'.$mode.'='.$module.'&alert=save');	
	}else{
	save_alert('error',$insert->error);
	}
    break;
	}
}
 ?>