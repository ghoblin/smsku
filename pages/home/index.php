﻿<?php 
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
header('location:../index.php');
}else{
switch($act){
  // Tampil List berita
  default:
?>
	  <div class="container-fluid">
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
					<a href="?module=inbox">
                        <div class="icon">
                            <i class="material-icons">inbox</i>
                        </div>
					</a>
                        <div class="content">
                            <div class="text">INBOX</div>
                            <div class="number" data-speed="1000" data-fresh-interval="20"><div id="inbox">0</div></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
					<a href="?module=send">
                        <div class="icon">
                            <i class="material-icons">send</i>
                        </div></a>
                        <div class="content">
                            <div class="text">SENT</div>
                            <div class="number" data-speed="1000" data-fresh-interval="20"><div id="kirimt">0</div></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">network_cell</i>
                        </div>
                        <div class="content">
                            <div class="text">SIGNAL</div>
                            <div class="number" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"><div id="signal">0</div></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 modem">
                    
                </div>
<!-- Line Chart -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>CHART PESAN MASUK</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="mycanvas" height="150"></canvas>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>CHART PESAN TERKIRIM</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="my_canvas" height="150"></canvas>
                        </div>
                    </div>
                </div>
            </div>
<!-- Hover Expand Effect -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-expand-effect">
                        <div class="icon bg-red">
                            <i class="material-icons">error</i>
                        </div>
                        <div class="content">
                            <div class="text">SMS GAGAL</div>
                            <div class="number" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"><div id="gagal">0</div></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-expand-effect">
					<a href="?module=schedule">
                        <div class="icon bg-green">
                            <i class="material-icons">schedule</i>
                        </div>
                        <div class="content">
                            <div class="text">SMS TERJADWAL</div>
                             <div class="number" data-speed="1000" data-fresh-interval="20"><div id="cast">0</div></div>
                        </div>
					</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-expand-effect">
					<a href="?module=kontak">
                        <div class="icon bg-light-green">
                            <i class="material-icons">contact_phone</i>
                        </div>
                        <div class="content">
                            <div class="text">KONTAK</div>
                            <div class="number" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"><div id="kontak">0</div></div>
                        </div>
						</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-expand-effect">
					<a href="?module=email">
                        <div class="icon bg-lime">
                            <i class="material-icons">mail</i>
                        </div>
                        <div class="content">
                            <div class="text">EMAIL</div>
                           <div class="number" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"><div id="grup">0</div></div>
                        </div>
                    </a>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Expand Effect -->
            <!-- #END# Widgets -->
            </div>
<?php
break;
case "edit":
$id = filter_var($_POST['iduser'], FILTER_SANITIZE_NUMBER_INT);
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
  $email = $_POST['email'];
} else {
  echo("$email is not a valid email address");
}
$password = md5($_POST['password']);
$nama = filter($mysqli,$_POST['nama']);
if($_POST['password']==""){
$sqlup = "UPDATE user set email='$email', nama='$nama' where id_user='$id'";
if ($mysqli->query($sqlup) === TRUE) {
echo "Data berhasil di edit";
htmlRedirect('?'.$mode.'='.$module.'&alert=save');	
} else {
echo "Error: " . $sqlup . "<br>" . $mysqli->error."'";
}
$mysqli->close();
}else{
$sqlup = "UPDATE user set pass='$password', email='$email', nama='$nama' where id_user='$id'";
if ($mysqli->query($sqlup) === TRUE) {
echo "Data berhasil di edit";
htmlRedirect('?'.$mode.'='.$module.'&alert=save');	
} else {
echo "Error: " . $sqlup . "<br>" . $mysqli->error."'";
}
$mysqli->close();
}
break;
case "updateinfo":
$id = filter_var($_POST['iduser'], FILTER_SANITIZE_NUMBER_INT);
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
  $email = $_POST['email'];
} else {
  echo("$email is not a valid email address");
}
$nama = filter($mysqli,$_POST['nama']);
$pembuat = filter($mysqli,$_POST['pembuat']);
$telp = filter($mysqli,$_POST['telp']);
$sqlup = "UPDATE info set email='$email', nama_perusahaan='$nama', nama_pemilik='$pembuat', telp='$telp'where id_info='$id'";
if ($mysqli->query($sqlup) === TRUE) {
echo "Data berhasil di edit";
htmlRedirect('?'.$mode.'='.$module.'&alert=save');	
} else {
echo "Error: " . $sqlup . "<br>" . $mysqli->error."'";
}
$mysqli->close();
break;

case "save_info":
	$email	= sanitize($_POST['email']);
	$toEmailAddress2	= sanitize($_POST['txtToCC']);
	foreach ($email as $status) {
	$strTo = $status;

$toEmailFull2 = "<$toEmailAddress2>";
$strSubject = $_POST["txtSubject"];
// $strDate = date("d-m-Y : H:i:s");
$strIsi = nl2br($_POST["txtDescription"]);
include  "pages/home/body-mail.php";
$body_code = base64_encode($strBody);
$SQL = $mysqli->query("INSERT INTO mail SET  judul='".$strSubject."', isi='".$body_code."', email='".$strTo."'");
//*** Uniqid Session ***//
$strSid = md5(uniqid(time()));

$strHeader = "";
$strHeader .= "From: ".sanitize($_POST["txtFormName"])."<".sanitize($_POST["txtFormEmail"]).">\nReply-To: ".sanitize($_POST["txtFormEmail"])."";

$strHeader .= "MIME-Version: 1.0\n";
$strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\n\n";
$strHeader .= "This is a multi-part message in MIME format.\n";

$strHeader .= "--".$strSid."\n";
$strHeader .= "Content-type: text/html; charset=utf-8\n";
$strHeader .= "Content-Transfer-Encoding: 7bit\n\n";
//$strHeader .= $strMessage."\n\n";
$strHeader .= $strBody."\n\n";

//*** Attachment ***//
if($_FILES["fileAttach"]["name"] != "")
{
$strFilesName = $_FILES["fileAttach"]["name"];
$strContent = chunk_split(base64_encode(file_get_contents($_FILES["fileAttach"]["tmp_name"])));
$strHeader .= "--".$strSid."\n";
$strHeader .= "Content-Type: application/octet-stream; name=\"".$strFilesName."\"\n";
$strHeader .= "Content-Transfer-Encoding: base64\n";
$strHeader .= "Content-Disposition: attachment; filename=\"".$strFilesName."\"\n\n";
$strHeader .= $strContent."\n\n";
}

$flgSend = @mail($strTo,$strSubject,null,$strHeader);  // @ = No Show Error //
}
if($flgSend)
{
save_alert('update',sukses);
htmlRedirect('?'.$mode.'='.$module);	
}
else
{
save_alert('update',gagal);
htmlRedirect('?'.$mode.'='.$module);	
}
	}//end switch
}//end