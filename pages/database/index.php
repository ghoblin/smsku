﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
switch($act){
  // Tampil List berita
  default:

?>
  <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATABASE
                            </h2>
                            <ul class="header-dropdown m-r--5 m-t--10">
                                <li>
                                    <a href="#" class="btn bg-green" onClick="showModalsg()">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Tambah kontak">add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
						<table id="js-basic" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                               <th style="width:1% !important;" >No</th>
                                               <th>Nama File</th>
                                               <th style="width:20%;text-align:center">Tanggal</th>
                                               <th style="width:5%;text-align:center">Aksi</th>
                                            </tr>
                                        </thead>
						<tbody>
<?php
$path = "backup/";
$files = scandir($path);
rsort($files);
$no = 1;
foreach ($files as $file) {
    if ($file != "." && $file != ".." && strpos($file, ".sql",1) && $s = explode("-",$file)) {
        echo "<tr>
		<td align='center'>".$no++."</td>
		<td><a href=\"$path$file\">$file</a></td>
		<td>".@date('d M Y H:i:s',$s[2])."</td>
		<td><a data-href='?module=database&act=hapus&file=$file' data-toggle='modal' data-target='#confirm-delete' href='#' data-toggle='tooltip' title='Hapus Data'><i class='material-icons' data-toggle='tooltip' data-placement='left' title='Tambah kontak'>delete</i></a></td>
		</tr>";
    }
}
?>
										</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
        </div>
		<!-- For Material Design Colors -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content modal-col-red">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
					</div>
					<div class="modal-body">
					<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
					<p>Apakah Anda ingin melanjutkan?</p>
					</div>
					<div class="modal-footer">
					   <a href="#" class="btn btn-danger danger">HAPUS</a>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
					</div>
				</div>
			</div>
		</div>

<?php
    break;
	case "hapus":
$path = "backup/";
	if(unlink($path.$_GET['file'])){
save_alert('delete',delete);
htmlRedirect('?'.$mode.'='.$module);
	}else{
save_alert('delete',errordel);
htmlRedirect('?'.$mode.'='.$module);
}
    break;
	}
}
 ?>