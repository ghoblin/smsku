﻿<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
//error_reporting(0);
// session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
switch($act){
  // Tampil List berita
  default:

?>
  <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA Template SMS
                            </h2>
                            <ul class="header-dropdown m-r--5 m-t--10">
                                <li>
                                    <a href="#" class="btn bg-green" onClick="showModalst()">
                                        <i class="material-icons" data-toggle="tooltip" data-placement="left" title="Tambah Template">add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
						<table id="js-thm" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                               <th style="width:1% !important;" >No</th>
                                               <th>Judul</th>
                                               <th>Isi</th>
                                               <th style="width:10%;text-align:center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
        </div>
		<!-- Modal add -->
		<div class="modal fade" id="myModalsT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabelT">Add Data</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger" role="alert" id="removeWarning">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Anda yakin ingin menghapus data ini
						</div>
						<br>
						<form id="formUserT">
							<input type="hidden" class="form-control" id="id" name="id">
							<input type="hidden" class="form-control" id="type" name="type">
				<div class="body">
					<div class="row clearfix">
						<div class="col-md-12">
							<b>Judul <span class="errorn" style="display:none;color:#ff0000;"></span></b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">contact_phone</i>
								</span>
								<div class="form-line">
					<input type="text" id="judul" name="judul" class="form-control" placeholder="Judul" required />
								</div>
							</div>
							<b>Custom SMS <span class="errorn" style="display:none;color:#ff0000;"></span></b>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">message</i>
								</span>
								<div class="form-line">
					<input type="text" id="isi" name="isi" class="form-control" placeholder="isi" required />
								</div>
							</div>
						</div>
					</div>
				</div>
						</form>
						
					</div>
					<div class="modal-footer">
						<button type="button" onClick="submitUsert()" class="btn btn-default">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- For Material Design Colors -->
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content modal-col-red">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
					</div>
					<div class="modal-body">
					<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
					<p>Apakah Anda ingin melanjutkan?</p>
					</div>
					<div class="modal-footer">
					   <a href="#" class="btn btn-danger danger">HAPUS</a>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
					</div>
				</div>
			</div>
		</div>

<?php
    break;
	}
}
 ?>