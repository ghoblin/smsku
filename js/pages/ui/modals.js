$(function () {
    $('.js-modal-buttons .btn').on('click', function () {
        var color = $(this).data('color');
        $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#mdModal').modal('show');
    });
	$('.modal-siar').on('click', function () {
        var color = $(this).data('color');
        $('#ModalSiaran .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#ModalSiaran').modal('show');
    });
});