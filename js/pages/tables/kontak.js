$(function () {
// $('#g-kontak').DataTable();
$('#g-kontak').DataTable({
	"bPaginate": true,
	"bLengthChange": false,
	"bFilter": false,
	"bSort": false,
	"bInfo": false,
	"bAutoWidth": false
});
// $('#js-kontak').DataTable();
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD HH:mm',
        clearButton: true,
        weekStart: 1
    });
});