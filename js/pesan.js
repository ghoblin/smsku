$(function(){
    $('.search-box input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var term = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(term.length){
            $.get("json/search.php", {query: term}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });

    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
      $('#insert_form').on("submit", function(event){
           event.preventDefault();  
           if($('#nomorhpp').val() == "")  
           {  
               $('.error-no').html('masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
           }  
           else if($('#pesan').val() == '')  
           {  
               $('.error').html('masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
           }   
           else  
           {
                $.ajax({
                     url:"json/kirim_pesan.php",  
                     method:"POST",  
                     data:$('#insert_form').serialize(),  
                     beforeSend:function(){
                          $('#insert').val("Inserting");  
                     },  
                     success:function(data){
                          $('#insert_form')[0].reset();  
                          $('#mdModal').modal('hide');
						  notify('white');
                     }  
                });  
           }  
      });  
//grup sms
$("#group_sms").on("submit", function(e) {
	var postData = $(this).serializeArray();
	var formURL = $(this).attr("action");
	$.ajax({
		url: formURL,
		type: "POST",
		data: postData,
		cache: false,  
		success: function(data, textStatus, jqXHR) {
			// $('#ModalSiaran .modal-header .modal-title').html("Result");
			// $('#ModalSiaran .modal-body').html(data);
			$('#mdModal').modal('hide');
			smsnotif('white',data);
		},
		
		error: function(jqXHR, status, error) {
			console.log(status + ": " + error);
		}
	});
	e.preventDefault();
});
$("#submitForm").on('click', function() {
var grups = $("#grups").val();
var pesans = $("#pesans").val();
if(grups==''){
	$('.errorg').html('belum dipilih').delay(200).fadeIn().delay(3000).fadeOut();
}else if(pesans==''){
	$('.error').html('masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
}else{
	$("#group_sms").submit();
}
});
$('#ModalSiaran').on('shown.bs.modal', function () {
	$("#pesans").val("").removeAttr( "disabled" );
});
});
//profile
$('#EditProfile').on('show.bs.modal', function (e) {
	var profileid = $(e.relatedTarget).data('id');
	$.ajax({
		type : 'POST',
		url : 'json/edit_profile.php', //Here you will fetch records 
		data :  'profileid='+ profileid, //Pass $id
		beforeSend: function(){
		$("#loadprof").html('<img src="images/loading.gif" />');
		},
		success : function(data){
		$('#loadprof').hide();
		$('.edit-profile').html(data);//Show fetched data from database
		}
	});
});
//info
$('#EditInfo').on('show.bs.modal', function (e) {
	var infoid = $(e.relatedTarget).data('id');
	$.ajax({
		type : 'POST',
		url : 'json/edit_info.php', //Here you will fetch records 
		data :  'infoid='+ infoid, //Pass $id
		beforeSend: function(){
		$("#loadinfo").html('<img src="images/loading.gif" />');
		},
		success : function(data){
		$('#loadinfo').hide();
		$('.edit-info').html(data);//Show fetched data from database
		}
	});
});
	 $('#BacaModalc').on('show.bs.modal', function (e) {
        var bacaid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'POST',
            url : 'pages/inbox/crud.php', //Here you will fetch records 
            // data :  'bacaid='+ bacaid, //Pass $id
			data: {id:bacaid,type:"baca"},
			beforeSend: function(){
				$("#loadin").html('<img src="images/loading.gif" />');
			},
            success : function(data){
            $('.baca-datain').html(data);//Show fetched data from database
			$('#loadin').hide();
			dTable.ajax.reload();
            }
        });
     });


function loadlinka(){
	$.ajax({
      url: 'json/api.php?signal=true',   
	  method: "GET",	  
      data: "",
      dataType: 'json',  
      success: function(data) 
      {
        var id = data[0];      
        $('#signal').html(id);    
      }
    });
	 setTimeout("loadlinka()",10000);
}
function loadlinkb(){
	$.ajax({
      url: 'json/api.php?signal=true',   
	  method: "GET",	  
      data: "",
      dataType: 'json',  
      success: function(data) 
      {
        var id = data[0];      
        $('#signala').html(id);    
      }
    });
	 setTimeout("loadlinkb()",10000);
}
function myFunction() {
    var val = document.getElementById("mySelect").value;
    document.getElementById("pesan").innerHTML = val;
}
function myFunctiong() {
    var val = document.getElementById("mySelectg").value;
    document.getElementById("pesans").innerHTML = val;
}

function countChar(val){
	var len = val.value.length;
	if (len > 160) {
	  val.value = val.value.substring(0, 160);
	} else {
	  $('#charNum').text(160 - len);
	}
};

  function tog(v){return v?'addClass':'removeClass';}
  $(document).on('input', '.clearable', function(){
    $(this)[tog(this.value)]('x');
  }).on('mousemove', '.x', function( e ){
    $(this)[tog(this.offsetWidth-24 < e.clientX-this.getBoundingClientRect().left)]('onX');   
  }).on('click', '.onX', function(){
    $(this).removeClass('x onX').val('').change();
  });
  

      $('#insert_email').on("submit", function(event){
           event.preventDefault();  
		   var email = $("#email").val();
           if(email == '')
           {
               $('.errorE').html('masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
           }
		   else if($('#txtSubject').val() == ''){
			   $('.errorJ').html('masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
		   }
           else if($('#txtDescription').val() == '')  
           {
               $('.errorP').html('masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
           }   
           else  
           {
			   var formdata = new FormData(this);
				// var formdata = $(this).serializeArray();
                $.ajax({
					url:"json/send.php",  
					method:"POST",   
					data: formdata,
					mimeTypes:"multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
                    beforeSend:function(data){
                          $('#insertMail').html("Sedang Mengirim");
                     },  
                     success: function(data, textStatus, jqXHR) {
						 if(data==0){
						  $('#insertMail').html("Kirim");  
                          $('#insert_email')[0].reset();  
                          $('#kirim-email').modal('hide');
						  $("#email").val("");
						  mailnotify('white');
						 }else if(data==1){
							 $("#removeWarning").html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Invalid Email, please provide an correct email').show().delay(200).fadeIn().delay(3000).fadeOut();
							 $('#insertMail').html("Kirim");  
						 }else if(data==2){
							 $("#removeWarning").html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Cek Koneksi Internet').show().delay(200).fadeIn().delay(3000).fadeOut();
							 $('#insertMail').html("Kirim");  
						 }else{
							  $("#removeWarning").html(data).show();
						 }
                     },
						error: function(jqXHR, status, error) {
						console.log(status + ": " + error);
						}
                });  
           }  
      });  