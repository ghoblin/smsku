			var xTable;
			// #Example adalah id pada table
			$(document).ready(function() {
				xTable = $('#js-sch').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": true,
					"sAjaxSource": "pages/schedule/serverSide.php", // Load Data
					"sServerMethod": "POST",
					"columnDefs": [
					{ "orderable": false, "targets": 0, "searchable": false },
					{ "orderable": true, "targets": 1, "searchable": true },
					{ "orderable": true, "targets": 2, "searchable": true },
					{ "orderable": true, "targets": 3, "searchable": true },
					{ "orderable": true, "targets": 4, "searchable": true }
					]
				} );
			} );
			
		    //Tampilkan Modal 
			function showModals( id )
			{
				waitingDialog.show();
				clearModals();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if( id )
				{
					$.ajax({
						type: "POST",
						url: "pages/schedule/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalData( res );
						}
					});
					// alert(id);
				}
				// Untuk Tambahkan Data
				else
				{
					$("#myModals").modal("show");
					$("#myModalLabel").html("Schedule Baru");
					$("#type").val("new"); 
					waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalData( data )
			{
				$("#myModalLabel").html("EDIT Schedule");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#nomorhp").val(data.nomorhp);
				$("#pesansc").val(data.pesansc);
				$("#time").val(data.time);
				$("#myModals").modal("show");
			}
			
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUser()
			{
			var nomorhp = $("#nomorhp").val();
			var pesansc = $("#pesansc").val();
			var time = $("#time").val();
			if(nomorhp==''){
			$('.errorn').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else if(pesansc==''){
			$('.errorp').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else if(time==''){
			$('.errort').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUser").serialize();
				waitingDialog.show();
				$.ajax({
					type: "POST",
					url: "pages/schedule/crud.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
						xTable.ajax.reload(); // Untuk Reload Tables secara otomatis
						waitingDialog.hide();	
						$('#myModals').modal('hide');
					}
				});
			}
			}
			
			//Hapus Data
			function deleteUser( id )
			{
				clearModals();
				$.ajax({
					type: "POST",
					url: "pages/schedule/crud.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						$("#myModalLabel").html("Hapus Data");
						$("#id").val(data.id);
						$("#type").val("delete");
						// $("#formUser").val(data.nomorhp).hide();
						$("#nomorhp").val(data.nomorhp).attr("disabled","true");
						$("#pesansc").val(data.pesansc).attr("disabled","true");
						$("#time").val(data.time).attr("disabled","true");
						$("#myModals").modal("show");
						waitingDialog.hide();			
					}
				});
			}
			
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModals()
			{
				$("#removeWarning").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#nomorhp").val("").removeAttr( "disabled" );
				$("#pesansc").val("").removeAttr( "disabled" );
				$("#time").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
		