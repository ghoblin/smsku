			var TableG;
			// #Example adalah id pada table
			$(document).ready(function() {
				TableG = $('#js-group').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": true,
					"sAjaxSource": "pages/group/data.php", // Load Data
					"columnDefs": [
					{ "orderable": false, "targets": 0, "searchable": false},
					{ "orderable": true, "targets": 1, "searchable": true }
					]
				} );
			} );
			
		    //Tampilkan Modal 
			function showModalsg( id )
			{
				waitingDialog.show();
				clearModalsg();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "POST",
						url: "pages/group/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalDatag( res );
						}
					});
					// alert(id);
				}
				// Untuk Tambahkan Data
				else
				{
					$("#myModalsG").modal("show");
					$("#myModalLabelG").html("Kontak Baru");
					$("#type").val("new"); 
					waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalDatag( data )
			{
				$("#myModalLabelG").html("EDIT Kontak");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#nama").val(data.nama);
				$("#myModalsG").modal("show");
			}
			
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUserg()
			{
			var nama = $("#nama").val();
			if(nama==''){
			$('.errorn').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUserG").serialize();
				waitingDialog.show();
				$.ajax({
					type: "POST",
					url: "pages/group/crud.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
						waitingDialog.hide();	
						if(data=='ok'){
						TableG.ajax.reload(); // Untuk Reload Tables secara otomatis
						$('#myModalsG').modal('hide');
						}else{
							 $('#error').html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+data+' !</div>').delay(200).fadeIn().delay(3000).fadeOut();
							 // $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+data+' !</div>');
						 }
					}
				});
			}
			}
			//Hapus Data
			function deleteUserg(id)
			{
				clearModalsg();
				$.ajax({
					type: "POST",
					url: "pages/group/crud.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						// $("#error").show();
						$("#myModalLabelG").html("Hapus Kontak");
						$("#id").val(data.id);
						$("#nama").val(data.nama).attr("disabled","true");
						$("#type").val("delete");
						$("#myModalsG").modal("show");
						waitingDialog.hide();	
					}
				});
			}
			
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalsg()
			{
				
				// $("#error").hide();
				$("#removeWarning").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#nama").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
		