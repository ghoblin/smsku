$(document).ready(function() {
	 var dTable;
		dTable = $('#js-table').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "pages/send/data.php",
		"aaSorting": [[ 0, "desc" ]],
		"bSort": true
		
    });
    $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });
     
    $('#deleteTriger').on("click", function(event){ // triggering delete one by one
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function(){
                if($(this).is(':checked')) { 
                    ids.push($(this).val());
                }
            });
            var ids_string = ids.toString();  // array to string conversion 
            $.ajax({
                type: "POST",
                url: "pages/send/crud.php",
                data: {data_ids:ids_string,type:"del"},
                success: function(result) {
                    // dTable.ajax.reload();
					dTable.draw(); // redrawing datatable
                },
                async:false
            });
        }else{
			 $('#confirm-alert').modal('show');
			}
    }); 
  window.time = 0; //global declaration
    function autorefresh() {
        var isChecked = document.getElementById("bulkDelete").checked;
        if (isChecked == true) {
            time = setInterval(function () {
				dTable.ajax.reload();
               $('input:checkbox').removeAttr('checked');
            }, 20000);
        } else if (isChecked == false) {
             time = setInterval(function () {
				dTable.ajax.reload();
            }, 20000);
        }
    }
autorefresh();
$('#confirm-delete').on('show.bs.modal', function(e) {
	var rowid = $(e.relatedTarget).data('id');
	// $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
	var dell = $(this).find('.danger').attr('href');
	$('.debug-url').html('Delete URL: <strong>' + rowid + '</strong>');
	$('#dell-url').val(rowid);
})
$('#btnDelteYes').click(function () {
		var id = $('#dell-url').val();
        $.ajax({
            type : 'POST',
            url : 'pages/send/crud.php', //Here you will fetch records 
            // data :  'rowid='+ rowid, //Pass $id
			data: {id:id,type:"hapus"},
			beforeSend: function(){
				$("#loads").html('<img src="images/loading.gif" />');
				},
            success : function(data){
            // dTable.ajax.reload();
			dTable.draw(); // redrawing datatable
			 $('#confirm-delete').modal('hide');
			 $('#loads').hide();
            }
        });
});

	 $('#BacaModalS').on('show.bs.modal', function (e) {
        var bacaid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'POST',
            url : 'pages/send/crud.php', //Here you will fetch records 
            // data :  'bacaid='+ bacaid, //Pass $id
			data: {id:bacaid,type:"read"},
			beforeSend: function(){
				$("#loads").html('<img src="images/loading.gif" />');
				},
            success : function(data){
            $('.baca-data').html(data);//Show fetched data from database
			$('#loads').hide();
			dTable.ajax.reload();
            }
        });
     });
});