$(document).ready(function(){
	$.ajax({
		url: "json/json_chart.php?masuk=true",
		method: "GET",
		success: function(data) {
			// console.log(data);
			var jum = [];
			var tahun = [];


			for(var i in data) {
				jum.push(data[i].jum);
				tahun.push(data[i].tahun);
			}

			var chartdata = {
                labels: tahun,
                datasets: [{
                    label: "Pesan Masuk",
                    data: jum,
                    borderColor: 'rgba(0, 188, 212, 0.75)',
                    backgroundColor: 'rgba(0, 188, 212, 0.3)',
                    pointBorderColor: 'rgba(0, 188, 212, 0)',
                    pointBackgroundColor: 'rgba(0, 188, 212, 0.9)',
                    pointBorderWidth: 1
                }]
			};

			var ctx = $("#mycanvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
			});
		},
		error: function(data) {
			console.log(data);
		}
	});	
	
	$.ajax({
		url: "json/json_chart.php?terkirim=true",
		method: "GET",
		success: function(data) {
			// console.log(data);
			var jum1 = [];
			var tahun1 = [];


			for(var i in data) {
				jum1.push(data[i].jum1);
				tahun1.push(data[i].tahun1);
			}

			var chartdata = {
                labels: tahun1,
                datasets: [{
                        label: "Pesan Terkirim",
                        data: jum1,
                        borderColor: 'rgba(233, 30, 99, 0.75)',
                        backgroundColor: 'rgba(233, 30, 99, 0.3)',
                        pointBorderColor: 'rgba(233, 30, 99, 0)',
                        pointBackgroundColor: 'rgba(233, 30, 99, 0.9)',
                        pointBorderWidth: 1
                }]
			};

			var ctx = $("#my_canvas");

			var LineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata,
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
	
});
