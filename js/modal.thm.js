			var TableT;
			// #Example adalah id pada table
			$(document).ready(function() {
				TableT = $('#js-thm').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": true,
					"sAjaxSource": "pages/themes/data.php", // Load Data
					"columnDefs": [
					{ "orderable": false, "targets": 0, "searchable": false},
					{ "orderable": true, "targets": 1, "searchable": true }
					]
				} );
			} );
			
		    //Tampilkan Modal 
			function showModalst( id )
			{
				waitingDialog.show();
				clearModalst();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "POST",
						url: "pages/themes/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalDatat( res );
						}
					});
					// alert(id);
				}
				// Untuk Tambahkan Data
				else
				{
					$("#myModalsT").modal("show");
					$("#myModalLabelT").html("Template Baru");
					$("#type").val("new"); 
					waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalDatat( data )
			{
				$("#myModalLabelT").html("EDIT Template");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#judul").val(data.judul);
				$("#isi").val(data.isi);
				$("#myModalsT").modal("show");
			}
			
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUsert()
			{
			var judul = $("#judul").val();
			if(judul==''){
			$('.errorn').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUserT").serialize();
				waitingDialog.show();
				$.ajax({
					type: "POST",
					url: "pages/themes/crud.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
						TableT.ajax.reload();
						waitingDialog.hide();	
						$('#myModalsT').modal('hide');
					}
				});
			}
			}
			//Hapus Data
			function deleteUsert(id)
			{
				clearModalsg();
				$.ajax({
					type: "POST",
					url: "pages/themes/crud.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						$("#myModalLabelT").html("Hapus Template");
						$("#id").val(data.id);
						$("#judul").val(data.judul).attr("disabled","true");
						$("#isi").val(data.isi).attr("disabled","true");
						$("#type").val("delete");
						$("#myModalsT").modal("show");
						waitingDialog.hide();			
					}
				});
			}
			
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalst()
			{
				$("#removeWarning").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#judul").val("").removeAttr( "disabled" );
				$("#isi").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
		