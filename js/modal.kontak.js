			var TableK;
			var TableGr;
			// #Example adalah id pada table
			$(document).ready(function() {
				TableK = $('#js-kontak').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": true,
					"sAjaxSource": "pages/kontak/serverSide.php", // Load Data
					"sServerMethod": "POST",
					"order": [[ 0, "desc"]],
					// "columnDefs": [{ targets: 'no-sort', orderable: false }]
					"columnDefs": [{ "orderable": false, "targets":'no-sort', "searchable": false}]
				} );
				TableGr = $('#js-groups').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": false,
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false,
					"sAjaxSource": "pages/kontak/data.php", // Load Data
					"columnDefs": [{ "orderable": false, "targets": 0, "searchable": false}]
				} );
			});
			
		    //Tampilkan Modal 
			function showModalsk( id )
			{
				waitingDialog.show();
				clearModalsk();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "POST",
						url: "pages/kontak/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalDatak( res );
						}
					});
				}
				// Untuk Tambahkan Data
				else
				{
					$("#myModalsK").modal("show");
					$("#myModalLabel").html("Kontak Baru");
					$("#type").val("new"); 
					waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalDatak( data )
			{
				// $('#nomor').keypress(validateNumber);
				$("#myModalLabel").html("EDIT Kontak");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#negara").val(data.negara);
				$("#groupid").val(data.groupid);
				$("#nama").val(data.nama);
				$("#emails").val(data.email);
				$("#kode").val(data.kode);
				$("#nomor").val(data.nomor);
				$("#myModalsK").modal("show");
				
			}
			
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUserk()
			{
			var nama = $("#nama").val();
			var neg = $("#negara").val();
			var nomor = $("#nomor").val();
			var groupid = $("#groupid").val();
			if(nama==''){
			$('.errorn').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else if(neg==''){
			$('.errorneg').html('Belum dipilih').delay(200).fadeIn().delay(3000).fadeOut();
			}else if(nomor==''){
			$('.errorno').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else if(groupid==''){
			$('.errort').html('Belum dipilih').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUserK").serialize();
				waitingDialog.show();
				$.ajax({
					type: "POST",
					url: "pages/kontak/crud.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
						TableK.ajax.reload(); // Untuk Reload Tables secara otomatis
						waitingDialog.hide();	
						$('#myModalsK').modal('hide');
					}
				});
			}
			}
			
			//Hapus Data
			function deleteUserk(id)
			{
				clearModalsk();
				$.ajax({
					type: "POST",
					url: "pages/kontak/crud.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$(".hidex").hide();
						$("#removeWarning").show();
						$("#myModalLabel").html("Hapus Kontak");
						$("#id").val(data.id);
						// $("#formUser").val("").hide();
						$("#negara").val(data.negara).attr("disabled","true");
						$("#groupid").val(data.groupid).attr("disabled","true");
						$("#nama").val(data.nama).attr("disabled","true");
						$("#emails").val(data.email).attr("disabled","true");
						$("#kode").val(data.kode).attr("disabled","true");
						$("#nomor").val(data.nomor).attr("disabled","true");
						$("#type").val("delete");
						$("#myModalsK").modal("show");
						waitingDialog.hide();			
					}
				});
			}
			
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalsk()
			{
				$("#removeWarning").hide();
				$(".hidex").show();
				$("#id").val("").removeAttr( "disabled" );
				$("#negara").val("").removeAttr( "disabled" );
				$("#groupid").val("").removeAttr( "disabled" );
				$("#nama").val("").removeAttr( "disabled" );
				$("#emails").val("").removeAttr( "disabled" );
				$("#kode").val("").removeAttr( "disabled" );
				$("#nomor").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
// $("#nomor").keydown(function(event) {
        // // Allow: backspace, delete, tab, escape, enter and .
        // if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 107,  110]) !== -1 ||
             // // Allow: Ctrl+A
            // (event.keyCode == 65 && event.ctrlKey === true) ||
             // // Allow: Ctrl+C
            // (event.keyCode == 67 && event.ctrlKey === true) ||
             // // Allow: Ctrl+X
            // (event.keyCode == 88 && event.ctrlKey === true) ||
             // // Allow: home, end, left, right
            // (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // // let it happen, don't do anything
                 // return;
        // }
        // // Ensure that it is a number and stop the keypress
        // if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            // event.preventDefault();
        // }
    // });
function myFunctions() {
    var val = document.getElementById("negara").value;
    document.getElementById("kode").value= val;
}
function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 46
        || event.keyCode === 37 || event.keyCode === 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
};
