	var TableR;
$(document).ready(function() {
	 TableR = $('#js-auto').DataTable( {
        bProcessing: true,
        bServerSide: true,
        sAjaxSource: "pages/autoreplay/data.php",
        aaSorting: [[0, "desc"]]
    });
			} );
			
		    //Tampilkan Modal 
			function showModalsR( id )
			{
				waitingDialog.show();
				clearModalsR();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "POST",
						url: "pages/autoreplay/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalDataR( res );
						}
					});
					// alert(id);
				}
				// Untuk Tambahkan Data
				else
				{
					$("#myModalsR").modal("show");
					$("#myModalLabel").html("Format Baru");
					$("#type").val("new"); 
					waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalDataR( data )
			{
				$("#myModalLabel").html("EDIT Format");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#key1").val(data.key1);
				$("#key2").val(data.key2);
				$("#myModalsR").modal("show");
			}
			
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUserR()
			{
			var key1 = $("#key1").val();
			var key2 = $("#key2").val();
			if(key1==''){
			$('.error1').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else if(key2==''){
			$('.error2').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUserR").serialize();
				waitingDialog.show();
				$.ajax({
					type: "POST",
					url: "pages/autoreplay/crud.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
						TableR.ajax.reload(); // Untuk Reload Tables secara otomatis
						waitingDialog.hide();	
						$('#myModalsR').modal('hide');
					}
				});
			}
			}
			
			//Hapus Data
			function deleteUserR(id)
			{
				clearModalsR();
				$.ajax({
					type: "POST",
					url: "pages/autoreplay/crud.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						$("#myModalLabel").html("Hapus Format");
						$("#id").val(data.id);
						// $("#formUser").val("").hide();
						$("#key1").val(data.key1).attr("disabled","true");
						$("#key2").val(data.key2).attr("disabled","true");
						$("#type").val("delete");
						$("#myModalsR").modal("show");
						waitingDialog.hide();			
					}
				});
			}
			
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalsR()
			{
				$("#removeWarning").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#key1").val("").removeAttr( "disabled" );
				$("#key2").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
  // function tog(v){return v?'addClass':'removeClass';}
  // $(document).on('input', '.clearable', function(){
    // $(this)[tog(this.value)]('x');
  // }).on('mousemove', '.x', function( e ){
    // $(this)[tog(this.offsetWidth-24 < e.clientX-this.getBoundingClientRect().left)]('onX');   
  // }).on('click', '.onX', function(){
    // $(this).removeClass('x onX').val('').change();
  // });