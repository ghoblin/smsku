$(document).ready(function(){

		var id_cek = 0;
        $('.cek').on("click", function(){
			 $('#ajax_loader').show();
			 $(".pbcek").html('').hide();
            var url = "services/cek-koneksi.php";
            id_cek = this.id;
            if(id_cek != 0) {
			var title = $(this).data('title');
                $("#myModalLabelcek").html(title);
            } else {
                $("#myModalLabelcek").html("Tambah Modem");
            }
            $.post(url, {id: id_cek} ,function(data) {
                $(".pbcek").html(data).show();
				$('#ajax_loader').hide();
            });
			
        });
		var id_play = 0;
        $('.plays').on("click", function(){
			 $('#ajax_loader3').show();
			 $(".play-koneksi").html('').hide();
            var url = "services/play-koneksi.php";
            id_play = this.id;
            if(id_play != 0) {
				var title = $(this).data('title');
                $("#myModalLabelplay").html(title);
            } else {
                $("#myModalLabelplay").html("error");
            }
            $.post(url, {id: id_play} ,function(data) {
                $(".play-koneksi").html(data).show();
				$('#ajax_loader3').hide();
            });
			
        });
		var id_stop = 0;
        $('.stop').on("click", function(){
			 $('#ajax_loader_stop').show();
			 $(".stop-koneksi").html('').hide();
            var url = "services/stop-koneksi.php";
            id_stop = this.id;
            if(id_stop != 0) {
				var title = $(this).data('title');
                $("#myModalLabelstop").html(title);
            } else {
                $("#myModalLabelstop").html("error");
            }
            $.post(url, {id: id_stop} ,function(data) {
                $(".stop-koneksi").html(data).show();
				$('#ajax_loader_stop').hide();
            });
			
        });
		var id_bck = 0;
        $('.backup').on("click", function(){
			 $('#ajax_loaderb').show();
			 $(".backupdb").html('').hide();
            var url = "services/backup.php";
            id_bck = this.id;
            if(id_bck != 0) {
                $("#myModalLabelb").html("Backup Database");
            } else {
                $("#myModalLabelb").html("error");
            }
            $.post(url, {id: id_bck} ,function(data) {
                $(".backupdb").html(data).show();
				$('#ajax_loaderb').hide();
            });
			
        });

$('#confirm-delete').on('show.bs.modal', function(e) {
	$(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
	
	$('.debug-url').html('Delete URL: <strong>' + $(this).find('.danger').attr('href') + '</strong>');
})
});