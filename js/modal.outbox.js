			var TableO;
			// #Example adalah id pada table
			$(document).ready(function() {
				TableO = $('#js-outbox').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": true,
					"sAjaxSource": "pages/outbox/data.php", // Load Data
					"columnDefs": [
					{ "orderable": false, "targets": 0, "searchable": false},
					{ "orderable": true, "targets": 1, "searchable": true }
					]
				} );
			} );
			
		    //Tampilkan Modal 
			function showModalso( id )
			{
				waitingDialog.show();
				clearModalso();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "POST",
						url: "pages/outbox/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							waitingDialog.hide();
							setModalDatao( res );
						}
					});
					// alert(id);
				}
				// Untuk Tambahkan Data
				else
				{
					$("#myModalsO").modal("show");
					$("#myModalLabelO").html("Template Baru");
					$("#type").val("new"); 
					$("#submito").html("Simpan");
					waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalDatao( data )
			{
				$("#myModalLabelO").html("EDIT SMS");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#submito").html("Update");
				$("#onomor").val(data.onomor);
				$("#osms").val(data.osms);
				$("#myModalsO").modal("show");
			}
			
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUsero()
			{
			var onomor = $("#onomor").val();
			if(onomor==''){
			$('.errorn').html('Masih kosong').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUserO").serialize();
				waitingDialog.show();
				$.ajax({
					type: "POST",
					url: "pages/outbox/crud.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
						TableO.ajax.reload(); // Untuk Reload Tables secara otomatis
						waitingDialog.hide();	
						$('#myModalsO').modal('hide');
					}
				});
			}
			}
			//Hapus Data
			function deleteUsero(id)
			{
				clearModalso();
				$.ajax({
					type: "POST",
					url: "pages/outbox/crud.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						$("#myModalLabelO").html("Hapus SMS");
						$("#id").val(data.id);
						$("#onomor").val(data.onomor).attr("disabled","true");
						$("#osms").val(data.osms).attr("disabled","true");
						$("#type").val("delete");
						$("#submito").html("Hapus SMS");
						$("#myModalsO").modal("show");
						waitingDialog.hide();			
					}
				});
			}
			
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalso()
			{
				$("#removeWarning").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#onomor").val("").removeAttr( "disabled" );
				$("#osms").val("").removeAttr( "disabled" );
				$("#type").val("");
				$("#submito").val("");
			}
  window.time = 0; //global declaration
    function autorefresho() {
             time = setInterval(function () {
				TableO.ajax.reload();
            }, 10000);
        }
autorefresho();