$(document).ready(function(){

        var id_ch = 0;
        $('.edit,.tambah').on("click", function(){
            var url = "pages/changelogs/edit.form.php";
            id_ch = this.id;
            if(id_ch != 0) {
                $("#myModalLabelc").html("Update Data Changelogs");
            } else {
                $("#myModalLabelc").html("Tambah Data changelogs");
            }
            $.post(url, {id: id_ch} ,function(data) {
                $(".pb-edit").html(data).show();
            });
        });
//dell
        $('#confirm-delete').on('click', '.btn-ok', function(e) {
            var $modalDiv = $(e.delegateTarget);
			var url = "pages/changelogs/hapus.php";
            var id = $(this).data('recordId');
            $.post(url, {id: id},function(data) {
			$modalDiv.addClass('loading');
			// alert(id);
            setTimeout(function() {
				$modalDiv.addClass('loading');
                $modalDiv.modal('hide').removeClass('loading');
				window.location.reload(1);
				$("#recordId").remove();
            }, 1000)
            });

        });
        $('#confirm-delete').on('show.bs.modal', function(e) {
            var data = $(e.relatedTarget).data();
            $('.title', this).text(data.recordTitle);
            $('.btn-ok', this).data('recordId', data.recordId);
        });
});

		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});		
var track_page = 1; //track user scroll as page number, right now page number is 1
var loading  = false; //prevents multiple loads

load_contents(track_page); //initial content load

$(window).scroll(function() { //detect page scroll
	if($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled to bottom of the page
		track_page++; //page number increment
		load_contents(track_page); //load content	
		
	}
});		
//Ajax load function
function load_contents(track_page){
    if(loading == false){
		loading = true;  //set loading flag on
		$('.loading-info').show(); //show loading animation 
		$.post( 'pages/changelogs/fetch_pages.php', {'page': track_page}, function(data){
			loading = false; //set loading flag off once the content is loaded
			if(data.trim().length == 0){
				//notify user if nothing to load
				$('.loading-info').html("No more records!");
				return;
				
			}
			
			$('.loading-info').hide(); //hide loading animation once data is received
			$("#results").append(data); //append data into #results element
		
		}).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
			alert(thrownError); //alert with HTTP error
		})
	}
	
}