/*
Author: Pradeep Khodke
URL: http://www.codingcage.com/
*/

$('document').ready(function()
{ 
     /* validation */
	 $("#sign_in").validate({
      rules:
	  {
			password: {
			required: true,
			},
			user_login: {
            required: true,
            },
	   },
       messages:
	   {
            password:{
                      required: "isi kata sandi"
                     },
            user_login: "isi user login",
       },
	   submitHandler: LoginForm	
       });  
	   /* validation */
	   
	   /* login submit */
	   function LoginForm()
	   {		
			var data = $("#sign_in").serialize();
				
			$.ajax({
				
			type : 'POST',
			url  : 'cek_login.php',
			data : data,
			beforeSend: function()
			{	
				$(".error").fadeOut();
				$("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; proses ...');
			},
			success :  function(response)
			   {						
					if(response=="ok"){
						$("#btn-login").html('<img src="images/loading.gif" /> &nbsp; Proses ...');
						setTimeout(' window.location.href = "read.php?module=home"; ',500);
					}
					else{
					$(".error").fadeIn(1000, function(){
					$(".error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
					$("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Masuk');
					$('.msg').hide();
					});
					}
			  }
			});
				return false;
		}
	   /* login submit */
});