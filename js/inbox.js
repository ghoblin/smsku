$(document).ready(function() {
	var dTable;
	dTable = $('#jsontable').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "pages/inbox/data.php",
		"aaSorting": [[ 0, "desc" ]],
		"bSort": true
	});
// timer = setInterval(function () {
				// dTable.ajax.reload();
               // // $('input:checkbox').removeAttr('checked');
            // }, 3000);
    $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });
  // window.time = 0; //global declaration
var timer;
    function autorefresh() {
        var isChecked = document.getElementById("bulkDelete").checked;
        if (isChecked == true) {
		clearTimeout( timer );
        } else if (isChecked == false) {
             timer = setInterval(function () {
				dTable.ajax.reload();
            }, 20000);
        }
    }
    $('#deleteTriger').on("click", function(event){ // triggering delete one by one
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function(){
                if($(this).is(':checked')) {
                    ids.push($(this).val());
					// alert('zzz');
                }
            });
            var ids_string = ids.toString();  // array to string conversion 
            // $.ajax({
                // type: "POST",
                // url: "pages/inbox/crud.php",
                // data: {data_ids:ids_string,type:"del"},
                // success: function(result) {
					// dTable.draw(); // redrawing datatable
                // },
                // async:false
            // });
			$('#dell-urla').val(ids_string);
			$('#confirm-deleteA').modal('show');
        }else{
			 $('#confirm-alert').modal('show');
			}
    }); 
autorefresh();



$('#btnDelteAll').click(function () {
		var ids_string = $('#dell-urla').val();
            $.ajax({
                type: "POST",
                url: "pages/inbox/crud.php",
                data: {data_ids:ids_string,type:"del"},
                success: function(result) {
					dTable.draw(); // redrawing datatable
					$('#confirm-deleteA').modal('hide');
                },
                async:false
            });
});

document.getElementById('bulkDelete').addEventListener('click', autorefresh);
$('#confirm-delete').on('show.bs.modal', function(e) {
	var rowid = $(e.relatedTarget).data('id');
	// $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
	var dell = $(this).find('.danger').attr('href');
	$('.debug-url').html('Delete URL: <strong>' + rowid + '</strong>');
	$('#dell-url').val(rowid);
})
$('#btnDelteYes').click(function () {
		var delid = $('#dell-url').val();
        $.ajax({
            type : 'POST',
            url : 'pages/inbox/crud.php', //Here you will fetch records 
           data: {id:delid,type:"hapus"},
            success : function(data){
            // dTable.ajax.reload();
			dTable.draw(); // redrawing datatable
			 $('#confirm-delete').modal('hide');
            }
        });
});
    $('#ReplayModal').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'POST',
            url : 'pages/inbox/crud.php', //Here you will fetch records 
            // data :  'rowid='+ rowid, //Pass $id
			data: {id:rowid,type:"reply"},
			beforeSend: function(){
			$("#loadrep").html('<img src="images/loading.gif" />');
			},
            success : function(data){
			$('#loadrep').hide();
            $('.fetched-data').html(data);//Show fetched data from database
            }
        });
     });

	 $('#BacaModal').on('show.bs.modal', function (e) {
        var bacaid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'POST',
            url : 'pages/inbox/crud.php', //Here you will fetch records 
            // data :  'bacaid='+ bacaid, //Pass $id
			data: {id:bacaid,type:"baca"},
			beforeSend: function(){
			$("#loadbaca").html('<img src="images/loading.gif" />');
			},
            success : function(data){
			$('#loadbaca').hide();
            $('.baca-data').html(data);//Show fetched data from database
			dTable.ajax.reload();
            }
        });
     });
	 $('#TambahModal').on('show.bs.modal', function (e) {
		 // $('#phone').keypress(validateNumber);
        var tambahid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'POST',
            url : 'pages/inbox/crud.php', //Here you will fetch records 
            // data :  'tambahid='+ tambahid, //Pass $id
			data: {id:tambahid,type:"add"},
            success : function(data){
            $('.tambah-data').html(data);//Show fetched data from database
			// dTable.ajax.reload();
            }
        });
     });
});
function countCharrep(val) {
        var len = val.value.length;
        if (len > 160) {
          val.value = val.value.substring(0, 160);
        } else {
          $('#charNumrep').text(160 - len);
        }
      };
