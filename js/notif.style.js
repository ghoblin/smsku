function mailnotify(style) {
        $.notify({
            title: 'EMAIL Notification',
            text: 'EMAIL telah dikirim!',
            image: "<img src='images/Mail.png'/>"
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
    }
	function notify(style) {
        $.notify({
            title: 'SMS Notification',
            text: 'SMS telah dikirim!',
            image: "<img src='images/Mail.png'/>"
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
    }
	function notifyadd(style) {
        $.notify({
            title: 'Save Notification',
            text: 'Kontak telah dimasukan!',
            image: "<i class='material-icons col-red'>add</i>"
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
    }
    function delnotif(style,msg,img) {
        $.notify({
            title: 'Notification',
            text: msg,
            image: "<i class='material-icons col-red'>"+img+"</i>"
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
    }
	function smsnotif(style,msg) {
        $.notify({
            title: 'Pengiriman SMS',
            text: msg,
            image: "<i class='material-icons col-red'>send</i>"
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
    }