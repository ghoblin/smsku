﻿<?php
session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
include "../g-asset/conn_db.php";
    $id = $_POST['infoid']; //escape string
    if($id > 0) {
	$sql = $mysqli->query("SELECT * FROM info WHERE id_info='$id'");
	$data=$sql->fetch_array();
	$iduser = $data['id_info'];
	$namaapp = $data['nama_perusahaan'];
	$pembuat = $data['nama_pemilik'];
	$email = $data['email'];
	$telp = $data['telp'];
	$ket= "SAVE";
    }else{
	$iduser = "";
	$namaapp = "";
	$pembuat = "";
	$email = "";
	$telp = "";
	$ket= "TAMBAH";
	}
	$mysqli->close();
?>
    <form method="POST" action="?module=home&act=updateinfo">
        <div class="form-group" hidden="hidden">
            <label class="col-sm-2 col-sm-2 control-label">ID</label>
            <div class="col-sm-2">
                <input type="text" name="iduser" id="iduser" value="<?php echo $iduser; ?>">
            </div>
        </div>
		<div class="row clearfix">
			<div class="col-md-3 form-control-label">
				<label for="">Nama APP</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control" name="nama" value="<?php echo $namaapp; ?>">
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">Pembuat</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control" name="pembuat" id="pembuat" value="<?php echo $pembuat; ?>">
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">Email</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="email" class="form-control" name="email" id="email" value="<?php echo $email; ?>">
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">No.HP</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control" name="telp" id="telp" value="<?php echo $telp; ?>">
					</div>
				</div>
			</div>
		</div>
	<div class="modal-footer">
        <button type="submit" value="submit" class="btn btn-link waves-effect"><?=$ket;?></button>
		<button type="button" class="btn btn-link bg-red waves-effect" data-dismiss="modal">TUTUP</button>
	</div>
    </form>
<?php } ?>