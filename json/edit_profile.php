﻿<?php
session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
include "../g-asset/conn_db.php";
    $id = $_POST['profileid']; //escape string
    if($id > 0) {
	$sql = $mysqli->query("SELECT * FROM user WHERE id_user='$id'");
	$data=$sql->fetch_array();
	$iduser = $data['id_user'];
	$username = $data['username'];
	$nama_lengkap = $data['nama'];
	$email = $data['email'];
	$ket= "SAVE";
    }else{
	$iduser = "";
	$username = "";
	$nama_lengkap = "";
	$email = "";
	$ket= "TAMBAH";
	}
	$mysqli->close();
?>
    <form method="POST" action="?module=home&act=edit">
        <div class="form-group" hidden="hidden">
            <label class="col-sm-2 col-sm-2 control-label">ID</label>
            <div class="col-sm-2">
                <input type="text" name="iduser" id="iduser" value="<?php echo $iduser; ?>">
            </div>
        </div>
		<div class="row clearfix">
			<div class="col-md-3 form-control-label">
				<label for="">Username</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control"  value="<?php echo $username; ?>" readonly>
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">Password</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="password" class="form-control" name="password" id="password">
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">Email</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="email" class="form-control" name="email" id="email" value="<?php echo $email; ?>">
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">Nama Lengkap</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control" name="nama" id="nama" value="<?php echo $nama_lengkap; ?>">
					</div>
				</div>
			</div>
		</div>
	<div class="modal-footer">
        <button type="submit" value="submit" class="btn btn-link waves-effect"><?=$ket;?></button>
		<button type="button" class="btn btn-link bg-red waves-effect" data-dismiss="modal">TUTUP</button>
	</div>
    </form>
<?php } ?>