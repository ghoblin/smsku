<?php
//setting header to json
header('Content-Type: application/json');
date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
include "../g-asset/conn_db.php";
$tahun=date("Y");
$bulan=date("m");
$bln=array(1=> "Januari", "Februari", "Maret", "April", "Mei", 
                    "Juni", "Juli", "Agustus", "September", 
                    "Oktober", "November", "Desember");
					
//query to get data from the table
// $query = sprintf("SELECT count(*) as jum FROM inbox where YEAR(ReceivingDateTime)='$tahun'");
$query = sprintf("select count(ID) as jum, month(ReceivingDateTime) as bulan, year(ReceivingDateTime) as tahun from inbox GROUP BY bulan DESC");

//execute query
$result = $mysqli->query($query);
//loop through the returned data
$data = array();
	while ($row = mysqli_fetch_object($result)) {
		$data[]=array(
		'jum'=>$row->jum,
		'bulan'=>$bln[$row->bulan],
		'tahun'=>$bln[$row->bulan] . " " . $row->tahun, 
		);	
	}
$query2 = sprintf("Select count(ID) as jml, month(SendingDateTime) as bln, year(SendingDateTime) as thn from sentitems GROUP BY bln DESC");
$results = $mysqli->query($query2);
$data2 = array();
	while ($rowku = mysqli_fetch_object($results)) {	
		$data2[]=array(
		'jum1'=>$rowku->jml, 
		'tahun1'=>$bln[$rowku->bln] . " " . $rowku->thn, 
		);	
	}
//free memory associated with result
$result->close();

//close connection
$mysqli->close();
//now print the data
// print json_encode($data);
if (isset($_GET['masuk'])) {
print json_encode($data);
}
if (isset($_GET['terkirim'])) {
print json_encode($data2);
}