# SMSKU MULTI PHP SMS gateway#

**[Documentation](https://gitlab.com/ghoblin/sms_multi/wikis/home)**

FASILITAS :
- SMS MASUK
- SMS KELUAR
- SMS TERKIRIM
- SMS GAGAL
- SMS TERJADWAL
- EMAIL TERKIRIM
- KONTAK
- GRUP KONTAK
- LAPORAN
- DATABASE
- AUTOREPLAY
- PENGATURAN MODEM
- CHANGELOGS

Kebutuhan:
* Komputer Windows 7
* Modem GSM
* XAMPP

# Dokumentasi #

**Untuk intruksi instalasi dan dokumentasi baca [wiki](https://gitlab.com/ghoblin/sms_multi/wikis/home).**

# Screenshots #
HOME
![alt text](screenshots/home.png "Text message sent to SMS server")

SMS  terkirim:
![alt text](screenshots/terkirim.png "Text message sent to SMS server")

SMS masuk:
![alt text](screenshots/masuk.png "sms masuk")