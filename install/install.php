<?php 

// error_reporting(0); 
ob_start(); 

include "../g-asset/fungsi_baca.php";
// $lines = file('../versi.txt'); 
// foreach ($lines as $line_num => $line){
	// $versi = "{$line}"; 
// }
// $baris = file('install.txt'); 
// foreach ($baris as $line_num => $line){
	// $install = "{$line}"; 
// }
$versi = cari_file('../','versi'); 
$install = cari_file('','install');
if($install==1 ){
	header("Location:../index.php"); die();
}else{
?>
<!DOCTYPE html> 
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>SMS Gateway | Rangkasku.web.id</title>
<style>
* {-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}
body{margin:0;padding:0;font-family:sans-serif}
a{text-decoration:none}
p{padding:0 5%}
table{width:100%;border:1px solid black;border-collapse:collapse}
td{padding:5px}
.container{width:55%;margin:10px 30% 10px 25%;border:1px solid #E20C0C}
.judul_atas{font-size:30px;margin:15px 5% -10px 5%}
.pembatas{width:90%;margin:5%}
header{color:#FFF;background-color:#E20C0C;text-align:center;padding:10px 0} 
.judul{font-size:15px;text-align:center}
form{width:90%;margin:5%}
textarea{width:100%;height:250px;padding:10px;border:1px solid #c1c1c1;resize:none} 
input[type=text]{margin-bottom:20px;margin-top:10px;width:100%;padding:15px;border:1px solid #E20C0C;}
input[type=password]{margin-bottom:20px;margin-top:10px;width:100%;padding:15px;border:1px solid #E20C0C;}
input[type=submit]{margin-bottom:20px;width:100%;padding:15px;border:none;background-color:#E20C0C;color:#FFF;font-size:20px;cursor:pointer;}
#submit:hover{background-color:#3A3A3A}
.logo{width:210px;height:123px;margin:10px 20px 0 20px;float:left;background:url("../images/logo.png") no-repeat}

@media only screen and (min-width: 601px) and (max-width: 1200px) {
.container{width:80%;margin:10px 30% 10px 11%}
}
@media only screen and (min-width: 987px) and (max-width: 1200px) {
.logo{margin-top:1px}
}
@media only screen and (min-width: 751px) and (max-width: 762px) {
.logo{height:223px}
}
@media only screen and (min-width: 300px) and (max-width: 750px) {
.logo{margin:10px auto;float:none}
}
@media only screen and (min-width: 351px) and (max-width: 600px) {
.container{width:80%;margin:10px 30% 10px 11%}
input[type=submit]{padding:8px;}
input[type=text]{padding:8px;}
input[type=password]{padding:8px}
}
@media only screen and (max-width: 350px) {
.container{width:80%;margin:10px 30% 10px 11%}
input[type=submit]{padding:8px}
input[type=text]{padding:8px}
input[type=password]{padding:8px}
}
</style>
</head>
<body>
<div align="center"><noscript><div style="position:fixed; top:0px; left:0px; z-index:10000; height:100%; width:100%; background-color:#FFFFFF"><div style="padding:10px; font-family: Tahoma; font-weight: bold; font-size: 16px; background-color:#FFF000">JavaScript harus diaktifkan pada browser Anda untuk dapat melanjutkan Installasi SMS Gateway</div></div></noscript></div>
<div class="container">
<header>
	<div class="logo"></div>
    <div class="judul_atas">SMS Gateway <?php echo $versi;?></div>
    <p>SMS Gateway</p>
    <p>[ Made in Indonesia ]</p>
</header>
<?php
session_start();
$module = (isset($_GET['module']) ? $_GET['module'] : '');
if ($module=='install'){
	echo "<div class='pembatas'>
			<div class='judul'>Anda harus menyetujui Perjanjian Lisensi Pengguna Akhir ini<br />untuk dapat melanjutkan proses installasi</div>
		";

	// software license
	$lokasi = '../license.txt';
	$ukuran = filesize($lokasi);
	$buka = fopen($lokasi,'r');
	$isi = fread($buka,$ukuran);
	echo "<h3>EULA (End User License Agreement)</h3>
	<textarea readonly='readonly' disabled='disabled'>$isi</textarea>";
	fclose($buka);

	echo "</div>
	<form method='post' action='?module=setuju'>
	<input type='hidden' name='setuju' value='setuju'/>
	<input type='submit' id='submit' value='Setuju'>
	</form>";
}
elseif ($module=='setuju'){
	// Memeriksa Ketersediaan Variabel Form dengan Fungsi isset()
	// Memastikan bahwa halaman ini diakses dengan parameter POST
	if (isset($_POST['setuju']))
	{
		if($_POST['setuju']=='setuju'){
			session_start(); $_SESSION['check']='diizinkan'; //session_destroy();
			header('location:?module=check');
		}
		else{echo "<script> alert('Anda harus menyetujui perjanjian !');window.location='install'</script>";}
	}
	else
	{
		echo "<script> alert('Anda harus menyetujui perjanjian !');window.location='install'</script>";
	}
}
elseif ($module=='check'){

	// Memastikan bahwa halaman ini diakses dengan Session check
	if ($_SESSION['check']=='diizinkan')
	{
		echo "<div class='pembatas'>";
		echo "<div class='judul'>Selamat datang di halaman Installasi</div>";
		echo "
			<h3>Uji Kelayakan Sistem</h3>
			<label>Versi PHP :</label>
		";
		if(PHP_VERSION > 5.0){
			echo PHP_VERSION;
		}
		else{
			echo "Versi PHP anda tidak mendukung";
			echo PHP_VERSION;
		}
		echo "<br /><label>Ekstensi MySQLi : </label>";
			echo (extension_loaded('mysqli')?'ON':'OFF');
		echo "<br /><label>Ekstensi PDO MySQL : </label>";
			echo (extension_loaded('pdo_mysql')?'ON':'OFF');
		if (ini_get('register_globals')){
			echo "<br /><font color=red>Registor Global : ON<br /><em>(Celah keamanan terbuka, silakan ganti pengaturan file php.ini anda)</em></font>";
		}
		else{echo "<br />Register Global : OFF";}
		/*echo "<br /><label>mod_rewrite : </label>";
			$isEnabled = in_array('mod_rewrite', apache_get_modules());
			echo ($isEnabled) ? 'YES' : 'NO';*/
		if((PHP_VERSION > 5.0)&&(extension_loaded('mysqli'))&&(extension_loaded('pdo_mysql'))){
			// session_start(); 
			$_SESSION['install']='diizinkan';
			echo "<br /><br /><br /><a href='?module=step1'><input type='submit' id='submit' value='Lanjut'></a></div>";
		}
		else{echo "<br /><br /><font color=red>Proses installasi tidak dapat dilanjutkan, silakan upgrade Versi PHP anda!</font>";}
	}
	else
	{
		echo "<script> alert('Anda harus menyetujui perjanjian !');window.location='install'</script>";
	}
}
elseif ($module=='step1'){
	
	// Memastikan bahwa halaman ini diakses dengan Session install
	if ($_SESSION['install']=='diizinkan')
	{
		$url = str_replace('step1', '', $_SERVER['REQUEST_URI']); //hapus step1 dengan fungsi str_replace
		echo "	<div class='pembatas'>
			<div class='judul'>Selamat datang di halaman Installasi. Silakan lengkapi form di bawah :</div>
				</div>
			<form method='post' action='?module=step2'>
				<label>URL Website</label>
				<input type='text' name='url_baru' placeholder='http://' required /><br />
			<h3>Database</h3>
				<label>Server Database</label>
				<input type='text' name='server' value='localhost' placeholder='localhost' required /><br />
				<label>Username Database</label>
				<input type='text' name='username' required /><br />
				<label>Password Database</label>
				<input type='password' name='password'  /><br />
				<label>Nama Database</label>
				<input type='text' name='database' required /><br />

			<h3>Administrator</h3>
				<label>Username</label>
				<input type='text' name='admin' required /><br />
				<label>Password</label>
				<input type='password' name='adminpass' required /><br />
				<label>Ulangi Password</label>
				<input type='password' name='adminpass2' required /><br />
				<input type='submit' id='submit' value='Install'>
			</form>
		";
	}
	else
	{
		echo "<script> alert('Lengkapi dulu persyaratan sistem');window.location='?module=check'</script>";
	}
}
elseif ($module=='step2'){
	// Memeriksa Ketersediaan Variabel Form dengan Fungsi isset()
	// Memastikan bahwa halaman ini diakses dengan parameter POST
	if (isset($_POST['url_baru']) AND isset($_POST['server']) AND isset($_POST['username']) AND isset($_POST['password']) AND isset($_POST['database']) 
		AND isset($_POST['admin']) AND isset($_POST['adminpass']) AND isset($_POST['adminpass2']))
	{
		$url_trim=trim(strip_tags($_POST['url_baru']));
		$server=trim(strip_tags($_POST['server']));
		$username=trim(strip_tags($_POST['username']));
		$dbPassword=trim(strip_tags($_POST['password']));
		$database=trim(strip_tags($_POST['database']));
		$admin_filter=trim(strip_tags($_POST['admin']));

		// menghindari sql injection
		function antiinjection($data){
		  $filter = stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));
		  return $filter;
		}

		$url_baru = antiinjection($url_trim);
		$dbHost = antiinjection($server);
		$dbUser = antiinjection($username);
		$dbPass = antiinjection($dbPassword);
		$dbName = antiinjection($database);
		$admin = antiinjection($admin_filter);
		$adminpass = antiinjection(md5($_POST['adminpass']));
		$adminpass2 = antiinjection(md5($_POST['adminpass2']));

		$koneksi = mysqli_connect($dbHost, $dbUser, $dbPass);
		
		if (empty($url_baru)){
			echo "<script>alert('Anda belum mengisikan URL Website');window.history.back()</script>";
		}
		elseif (empty($dbHost)){
			echo "<script>alert('Anda belum mengisikan Server Database');window.history.back()</script>";
		}
		elseif (empty($dbUser)){
			echo "<script>alert('Anda belum mengisikan Username Database');window.history.back()</script>";
		}
		// elseif (empty($dbPass)){
			// echo "<script>alert('Anda belum mengisikan Password Database');window.history.back()</script>";
		// }
		elseif (empty($dbName)){
			echo "<script>alert('Anda belum mengisikan Nama Database');window.history.back()</script>";
		}
		elseif (empty($admin)){
			echo "<script>alert('Anda belum mengisikan Username Administrator');window.history.back()</script>";
		}
		// Memvalidasi username admin, hanya boleh berisi alpha-numeric (a-z, A-Z, 0-9), underscore, dan minimal berisi 5 karakter dan maksimum 20 karakter
		elseif (!preg_match('/^[a-z\d_]{5,20}$/i', $admin)){ // Anda bisa mengganti jumlah minimun dan maksimum karakter sesuai kebutuhan
			echo "<script>alert('Username admin hanya boleh kombinasi huruf, angka dan underscore, serta minimal berisi 5 karakter dan maksimum 20 karakter');window.history.back()</script>";
		}
		elseif (empty($_POST['adminpass'])){
			echo "<script>alert('Anda belum mengisikan Password Administrator');window.history.back()</script>";
		}
		elseif(strlen($_POST['adminpass']) < 4){ //menghitung jumlah karakter dengan strlen()
			echo "<script>alert('Password Administrator minimum 5 digit');window.history.back()</script>";
		}
		elseif ($adminpass!=$adminpass2){ // Pastikan bahwa password yang dimasukkan sebanyak dua kali sudah cocok
			echo "<script>alert('Password yang diulangi tidak sama');window.history.back()</script>";
		}
		elseif (!$koneksi){
			echo "<script>alert('Koneksi ke Database Gagal');window.history.back()</script>";
		}
		else{
		$directory = glob('../backup/*');
		foreach ($directory as $file) {
			$mdtime = filemtime($file);
		}
			echo "<div class='pembatas'>";
			$sql = "CREATE DATABASE IF NOT EXISTS $dbName"; // Membuat database
			if ($koneksi->query($sql) === TRUE) {
			  echo "<br />Database berhasil dibuat dengan MySQLi Extension<br />";
				// Restore Tabel
				$koneksi = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);
				$filename = "db-backup-$mdtime-073094ca9e900398ce02835582ebd956-$versi.sql";
				$templine = '';
				$lines = file($filename);
				foreach ($lines as $line)
				{
					if (substr($line, 0, 2) == '--' || $line == '')
						continue;
					$templine .= $line;
					if (substr(trim($line), -1, 1) == ';')
					{
						mysqli_query($koneksi, $templine);
						$templine = '';
					}
				}
						if(!$templine){
							echo "<br /><font color=red>Tabel gagal direstore, data tidak tersedia</font><br />";
						}
						else {
							echo "<br /><Tabel berhasil direstore<br />";
						}
			}
			else {
				echo "<br /><font color=red>Database gagal dibuat</font><br />";
			}

// Generate koneksi.php pada folder config
$mysqli 	= '$mysqli';
$gaSqlu 	= '$gaSql[\'user\']';
$gaSqlp 	= '$gaSql[\'password\']';
$gaSqld 	= '$gaSql[\'db\']';
$gaSqls 	= '$gaSql[\'server\']';
$gaSqlpo 	= '$gaSql[\'port\']';
$page 	 	= '$item_per_page';
$mysqliDebug ='$mysqliDebug';
$msg = "'<p>There was an error connecting to the database!</p>'";
$error = '$mysqli->connect_error';
chmod("../g-asset/conn_db.php",0775);
$f = fopen("../g-asset/conn_db.php", "w");
fwrite($f, 
"<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	exit('Direct access not permitted.');
}
	$mysqliDebug = true;
 	$gaSqlu 	= '$dbUser';
	$gaSqlp 	= '$dbPass';
	$gaSqld 	= '$dbName';
	$gaSqls 	= '$dbHost';
	$gaSqlpo 	= 3306;
	$page	 	= 3;
	
	$mysqli = @new mysqli($gaSqls,$gaSqlu,$gaSqlp,$gaSqld);
    if ($error) {
        echo $msg;
        if ($mysqliDebug) {
            echo $error;
        }
        die();
    }
define('BASE_URL', '$url_baru');

?>");
fclose($f);
chmod("../g-asset/conn_db.php",0444);
			echo"
				<h3>Pengaturan Database & Administrator</h3>
				<table border='1'>
					<tr><td>URL Website :</td><td>$url_baru</td></tr>
					<tr><td>Server Database :</td><td>$dbHost</td></tr>
					<tr><td>Username Database :</td><td>$dbUser</td></tr>
					<tr><td>Password Database :</td><td>*****</td></tr>
					<tr><td>Nama Database :</td><td>$dbName</td></tr>
					<tr><td>Username Administrator :</td><td>$admin</td></tr>
					<tr><td>Password Administrator :</td><td>*****</td></tr>
				</table>
				";

			echo "</div>";

			// Lanjutkan tugas bro
			if($koneksi){
				// session_start(); 
				$_SESSION['step3']='diizinkan';
						// Kirim info ke step3 untuk disampaikan ke Finish biar doi kenal $folder_baru
				echo "	<form method='post' action='?module=step3'>
						<a href='step3'><input type='submit' id='submit' value='Lanjutkan'></a>
						</form>";
			}else{
				echo "<div class='pembatas'><a onClick='self.history.back()'><input type='submit' id='submit' value='Kembali'></a></div>";
			}

			$koneksi->close(); //menutup koneksi

		}
	}
	else
	{
		echo "<script> alert('Anda belum mengisi semua form');window.location='?module=step1'</script>";
	}	
}
elseif ($module=='step3'){
	// session_start();
	// Memastikan bahwa halaman ini diakses dengan Session step3
	if ($_SESSION['step3']=='diizinkan')
	{
		include "../g-asset/conn_db.php";
		$sql=mysqli_fetch_array(mysqli_query($mysqli, "SELECT * FROM info where id_info='2'"));
		echo "	<div class='pembatas'>
			<div class='judul'>Lanjutkan proses installasi dengan mengisi pengaturan website anda.<br />
								Untuk mengganti pengaturan website atau yang lebih detail
								bisa anda akses pada halaman Administrator dengan Level Admin</div>
				</div>
			<form method='post' action='?module=step4'>
			<h3>Pengaturan Website</h3>
				<label>Nama Aplikasi</label>
				<input type='text' name='nama_web' value='$sql[1]' required /><br />
				<input type='submit' id='submit' value='Simpan'>
			</form>
		";
	}
	else
	{
		echo "<script> alert('Anda belum mengisi semua form');window.location='step1'</script>";
	}
}
elseif ($module=='step4'){
	// session_start();
	// Memeriksa Ketersediaan Variabel Form dengan Fungsi isset()
	// Memastikan bahwa halaman ini diakses dengan parameter POST
	if (isset($_POST['nama_web']))
	{
		$nw=trim(strip_tags($_POST['nama_web']));

		// menghindari sql injection
		function antiinjection($data){
		  $filter = stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));
		  return $filter;
		}

		$nama_web = antiinjection($nw);

		
		if (empty($nama_web)){
			echo "<script>alert('Anda belum mengisikan Nama Website');window.history.back()</script>";
		}
		else{
			// Update URL Website
			include "../g-asset/conn_db.php";
			mysqli_query($mysqli, "UPDATE info SET	site_title='$nama_web' where id_info='2'");
			echo"
			<div class='pembatas'>
			<h3>Pengaturan Aplikasi Anda</h3>
			<table border='1'>
				<tr><td>Nama Aplikasi :</td><td>$nama_web</td></tr>
			</table>
			</div>

			<form method='post' action='?module=finish'>
				<input type='hidden' name='finish' value='finish'/>
				<input type='submit' id='submit' value='Finish'>
			</form>
			";
		}
	}
	else
	{
		echo "<script> alert('Anda belum mengisi semua form');window.location='?module=step3'</script>";
	}
}
elseif ($module=='finish'){
	// Memeriksa Ketersediaan Variabel Form dengan Fungsi isset()
	// Memastikan bahwa halaman ini diakses dengan parameter POST
	if (isset($_POST['finish']))
	{
		// session_start(); 
		session_destroy();
		include "../g-asset/conn_db.php";
		$sql=mysqli_fetch_array(mysqli_query($mysqli, "SELECT * FROM info"));

		echo "<div class='pembatas'>
				<div class='judul'>Installasi SMS Gateway berhasil</div><br />";
		echo "<h3>Silakan hapus file instalasi untuk keamanan</h3><br />
				<a onClick=\"href='?module=lihatweb'\"><input type='submit' id='submit' value='Login'></a>
			  </div>";
	}
	else
	{
		echo "<script> alert('Anda belum mengisi semua form');window.location='step1'</script>";
	}
}
elseif ($module=='lihatweb'){
include "../g-asset/conn_db.php";
header('location:'.BASE_URL);
// $file = fopen("install.txt","w");
// echo fwrite($file,1);
// fclose($file);
}
?>
</div>
</body>
</html>
<?php 
}
ob_end_flush(); ?>
