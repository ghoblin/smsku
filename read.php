<?php
session_start();
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Maaf , Aplikasi SIANIDA tidak berjalan pada versi PHP yang lebih kecil dari 5.3.7!");
} else if (version_compare(PHP_VERSION, '5.6.0', '<')) {
    exit("Maaf , Aplikasi SIANIDA tidak berjalan pada versi PHP yang lebih kecil dari 5.6.0!");
}
include "g-asset/fungsi_baca.php";
$install = cari_file('install/','install');
if($install==0){
	header("Location:index.php"); die();
}else{
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
header('location:index.php');
}else{
	
	include __DIR__ . '/g-asset/conn_db.php';
	include __DIR__ . '/g-asset/web_function.php';
	include __DIR__ . '/g-asset/fungsi_ganti.php';
	include __DIR__ . '/g-asset/library_function.php';
	include __DIR__ . '/g-asset/functions.php';
	include __DIR__ . '/g-asset/data_mail.php';
	include __DIR__ . '/lib/lang.php';
	include 'services/function.php';
include "g-asset/fungsi_kompresi.php";


$mode = "module";
$module = (isset($_GET['module']) ? $_GET['module'] : '');
$act 	= (isset($_GET['act']) ? $_GET['act'] : '');
$idx 	= (isset($_GET['id']) ? $_GET['id'] : '');
$alert 	= (isset($_GET['alert']) ? $_GET['alert'] : '');
$status = (isset($_GET['status']) ? $_GET['status'] : '');
$step = (isset($_GET['step']) ? $_GET['step'] : '');
// $module = $_GET['module']; 
$stat = "status"; 
if ($act==''){
$bread = '';
}else{
$bread = ' / '.$act;
}
$status 	= (isset($_GET[$stat]) ? $_GET[$stat] : '');
// $status = $_GET[$stat]; 
ob_start("kompresi_output"); 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | <?php info('info',1,2);?></title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="css/roboto-font.css" rel="stylesheet" type="text/css">
    <!-- Material icon Css -->
    <link href="css/material-icon.css" rel="stylesheet">
    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Light Gallery Plugin Css -->
    <link href="plugins/light-gallery/css/lightgallery.css" rel="stylesheet">
    <!-- Preloader Css -->
    <link href="plugins/material-design-preloader/md-preloader.css" rel="stylesheet" />
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
	<!-- Multi Select Css -->
    <link href="plugins/chosen/chosen.css" rel="stylesheet">
    <!-- Bootstrap Spinner Css -->
    <link href="plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">
	<link href="dist/summernote.css" rel="stylesheet">
    <!-- Wait Me Css -->
    <link href="plugins/waitme/waitMe.css" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="css/dropify.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
	<link href="plugins/notify/styles/metro/notify-metro.css" rel="stylesheet" />
	<link rel="stylesheet" href="plugins/fastselect/fastselect.css">

</head>
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
	    
	    <div class="overlay"></div>
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="read.php?module=home"><?php info('info',1,2);?></a>
				
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Notifications -->
					<!-- Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#kirim-email" role="button">
                            <i class="material-icons" data-toggle="tooltip" title="Kirim Email" data-placement="left">mail</i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="tooltip" title="Signal" data-placement="left" role="button">
                            <i class="material-icons">signal_cellular_4_bar</i>
                            <span class="label-count" id="signala">0</span>
                        </a>
                    </li>
                    <!-- #END# Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count" data-speed="1000" data-fresh-interval="20"><div id="notif">0</div></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFIKASI SMS</li>
                            <li class="body">
                                <ul class="menu" id="cek_notif"></ul>
                            </li>
                            <li class="footer">
                                <a href="?module=inbox">Lihat Semua SMS</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
<?php include "sidebar.php"; ?>

<section class="content">
                    <!-- Main row -->
<?php 
$GETID = $idx;
$pathFile = "pages/".$module."/index.php";
if (file_exists($pathFile))
{
include __DIR__ . '/'.$pathFile;
}else {
?>
        <div class="container-fluid">
            <div class="block-header">
                <h2>ERROR PAGE</h2>
            </div>
        </div>
	<?php
}
?>

	</section><!-- /.content -->
    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

	
	<!-- Select Plugin Js -->
    <script src="js/dropify.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        <!-- chosen -->
	<script src="plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>
		<script src="plugins/fastselect/fastselect.standalone.js" type="text/javascript"></script>
    <!-- Input Mask Plugin Js -->
    <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
    <!-- Jquery Spinner Plugin Js -->
    <script src="plugins/jquery-spinner/js/jquery.spinner.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>
    <!-- Jquery Validation Plugin Css -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>
    <!-- JQuery Steps Plugin Js -->
    <script src="plugins/jquery-steps/jquery.steps.js"></script>
    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>
    <!-- Autosize Plugin Js -->
    <script src="plugins/autosize/autosize.js"></script>
    <!-- Moment Plugin Js -->
    <script src="plugins/momentjs/moment.js"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Custom Js -->
    <script src="js/jam.js"></script>
    <script src="js/admin.js"></script>
	<script src="js/pages/ui/tooltips-popovers.js"></script>
	<script src="js/pages/forms/form-wizard.js"></script>
    <script src="js/pages/ui/modals.js"></script>
	<script src="plugins/notify/notify.js"></script>
	<script src="plugins/notify/styles/metro/notify-metro.js"></script>
	<script src="js/pages/forms/form-validation.js"></script>
    <script src="js/countdata.js"></script>
	<?php
	if($module=='home' AND $act==''){ ?>
    <script src="js/home.js"></script>
    <script src="plugins/chartjs/Chart.min.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
	<?php }elseif($module=='outbox' OR $module=='kontak' OR $module=='schedule' OR $module=='changelogs' OR $module=='group' OR $module=='themes' OR $module=='autoreplay'){ ?>
	<script src="js/jquery.form.min.js"></script>
	<script src="js/modal.schedule.js"></script>
	<script src="js/modal.kontak.js"></script>
	<script src="js/modal.group.js"></script>
	<script src="js/modal.thm.js"></script>
	<script src="js/modal.autoreplay.js"></script>
	<script src="js/changelogs.js"></script>
	<script src="js/modal.outbox.js"></script>
	<script src="js/common.js"></script>
	<script src="js/pages/tables/kontak.js"></script>	
	<?php }elseif($module=='inbox'){ ?>
	<script src="js/inbox.js"></script>
	<?php }elseif($module=='send'){ ?>
	<script src="js/send.js"></script>
	<script src="js/pages/forms/advanced-form-elements.js"></script>
	<?php }elseif($module=='email'){ ?>
	<script src="js/mail.js"></script>
<?php
}
?>
<div class="fabx js-modal-buttons">
<button type="button" data-color="purple" class="btn bg-red btn-circle waves-effect waves-circle waves-float waves-light" data-toggle="tooltip" data-placement="left" title="FLASH SMS"><i class="material-icons col-white" >send</i>
</button>
</div>
<?php include"lib/modal.php"; ?>
    <script src="js/pesan.js"></script>
    <script src="js/pages/forms/basic-form-elements.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>
<script>

window.onload = function(event) {
cek_notif();notifx();ajax();loadlinka();loadlinkb();
inbox();kirimt();cast();gagal();kontak();grup();wsdl();outbox();sento();
};
$(document).on('shown.bs.modal', function(e) {
	$('[autofocus]', e.target).focus();
});
		$('#email').selectize({
		plugins: ['remove_button'],
		persist: true,
		create: true,
		render: {
		item: function(data, escape) {
			return '<div>' + escape(data.text) + '</div>';
					}
				}
			});
</script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <!-- Demo Js -->
    <script src="js/notif.js"></script>
    <script src="js/notif_wsdl.js"></script>
    <script src="js/demo.js"></script>
    <script src="js/notif.style.js"></script>

	<script type="text/javascript">
	$('li').each(function(){
	 if(window.location.href.indexOf($(this).find('a:first').attr('href'))>-1)
	 {$(this).addClass('active').siblings().removeClass('active');}
	});
	$('#chosentags').selectize({
	plugins: ['remove_button'],
	persist: true,
	create: true,
	render: {
	item: function(data, escape) {
		return '<div>' + escape(data.text) + '</div>';
				}
			}
		});
  </script>
    <script src="js/prosess.js"></script>
    <script src="js/cek-modem.js"></script>
	<?php if($module!='changelogs'){ ?>
    <script src="js/confirm.close.js"></script>
	<?php } ?>
</body>

</html>
<?php 
ob_end_flush();
}
}
 ?>