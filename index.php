﻿<?php
@ob_start();
session_start();
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Maaf , Aplikasi SMS GATEWAY tidak berjalan pada versi PHP yang lebih kecil dari 5.3.7!");
} else if (version_compare(PHP_VERSION, '5.6.0', '<')) {
    exit("Maaf , Aplikasi GATEWAY tidak berjalan pada versi PHP yang lebih kecil dari 5.6.0!");
}
include "g-asset/fungsi_baca.php";
$install = cari_file('install/','install');
if($install==0){
	header("Location: install/install.php?module=install"); die();
}else{
if (!empty($_SESSION['namauser']) AND !empty($_SESSION['passuser'])){
header('location:read.php?module=home');
}else{
include "g-asset/fungsi_kompresi.php";
ob_start("kompresi_output"); 
	?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | SMSGateway</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
<link href="min/f=smsku/css/roboto-font.css,smsku/css/material-icon.css,smsku/plugins/bootstrap/css/bootstrap.min.css,smsku/css/style.css" rel="stylesheet" type="text/css">
    <!-- Google Fonts --
    <link href="css/roboto-font.css" rel="stylesheet" type="text/css">
    <!-- Material icon Css --
    <link href="css/material-icon.css" rel="stylesheet">
	
	<!-- Bootstrap Core Css --
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Css --
    <link href="css/style.css" rel="stylesheet"-->
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin<b>SMS</b></a>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST">
                   <div class="error"></div>
				   <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="user_login" id="user_login" placeholder="User Login" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" id="password"  placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" name="btn-login" id="btn-login" type="submit">SIGN IN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>
    <!-- Custom Js -->
    <script src="js/script-log.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>
<?php } }

?>