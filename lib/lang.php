<?php
// error_reporting(0);
/**
* @version		v 1.0
* @package		SMS GATEWAY
* @copyright	Copyright (C) 2012 Rangkasku.
* @license		GNU/GPL version 3.0 (GPLv3), see license.txt
* @description	
**/

/************************************
*		  Global Translate 			*
*************************************/
define("Yes","Ya");
define("No","Tidak");
define("Save","Simpan");
define("Save_and_Quit","Simpan lalu keluar");
define("Apply","Simpan perubahan");
define("Delete","Hapus");
define("Delete_All","Hapus yang terpilih");
define("Cancel","Keluar");
define("Back","Kembali");
define("Next","Selanjutnya");
define("Prev","Kembali");
define("Click_for_helps","Klik untuk bantuan");
define("Click_to_edit","Klik untuk mengedit");
define("_Public","Publik");
define("Author","Author");
define("Version","Seri");
define("Sperator","Pembatas");
define("Name","Nama");
define("Address","Alamat");
define("City","Kota");
define("State","Provinsi");
define("Country","Negara");
define("Zip","Kode Pos");
define("Phone","Telepon");
define("Job","Jabatan");
define("Gender","Jenis Kelamin");
define("Man","Laki-laki");
define("Woman","Perempuan");
define("Category","Kategori");
define("Position","Posisi");
define("Short","Urutan");
define("Type","Tipe");
define("Date","Tanggal");
define("Description","Deskripsi");
define("Information","Informasi");
define("Group_Name","Nama Group");
define("Additional_Information","Informasi Tambahan");
define("Refresh","Tata ulang pengurutan");
define("Set_Disable","Jadikan tidak aktif");
define("Set_Enable","Aktifkan");
define("Show_title","Tampilkan Judul");
define("Show_title_tip","Tampilkan Judul");
define("Access_level","Level Akses");
define("Add_css_class","Tambahkan Class CSS");
define("CSS_Class_tip","Class CSS tambahan pada modul.");
define("CSS_Style_tip","Anda juga bisa menyisipkan style CSS disini.");
define("Set_disable","Non-aktifkan");
define("Set_enable","Aktifkan");
define("Access_Level","Level Akses");
define("Add_New_Item","Tambahkan data baru");
define("Themes_By","Themes by:Fiyo.Org | Diperbaharui Oleh : <a href=\"http://www.rangkasku.web.id\" title=\"Rangkasku.web.id\" class=\"tooltip\">Ibnu Kazol</a> Untuk Admin Lokomedia");
define("Pubilsh_item","Pubilsh yang Terpilih");
define("Unpublish_item","Unpublish yang Terpilih");
define("Helper","Bantuan");

/************************************
*			SAVE ALERT			*
************************************/
define("save","<b>Alert!</b> Data berhasil disimpan.",TRUE);
define("update","<b>Alert!</b> Data berhasil diperbaharui.",TRUE);
define("error","<b>Alert!</b> Data gagal disimpan.",TRUE);
define("errorstat","<b>Alert!</b> status error.",TRUE);
define("delete","<b>Alert!</b> Data berhasil dihapus.",TRUE);
define("del_id","<b>Alert!</b> Data tidak boleh dihapus.",TRUE);
define("fill","<b>Alert!</b> Data tidak boleh kosong.",TRUE);
define("cekbox","<b>Alert!</b> Data belum dipilih.",TRUE);
define("upload","<b>Alert!</b> Type file yang anda upload tidak sesuai/ file belum dipilih.",TRUE);
define("max","<b>Alert!</b> Type file yang anda upload tidak sesuai.",TRUE);
define("del_err","<b>Alert!</b> Data kategori tidak bisa di hapus, hapus artikel terlebih dahulu.",TRUE);
define("del_menu_err","<b>Alert!</b> Menu utama tidak bisa di hapus, hapus sub menu terlebih dahulu.",TRUE);
define("read","<b>Alert!</b> Menandai pesan sudah dibaca.",TRUE);
define("unread","<b>Alert!</b> Menandai pesan belum dibaca.",TRUE);
define("move","<b>Alert!</b> Pesan Berhasil di pindah ke Folder Junk.",TRUE);
define("inbox","<b>Alert!</b> Pesan Berhasil di pindah ke Folder inbox.",TRUE);
define("msg_delete","<b>Alert!</b> Pesan Berhasil dihapus.",TRUE);
define("gambar","<b>Alert!</b> Type yg diperbolehkan *.JPG",TRUE);
define("duplikat","<b>Alert!</b> Maaf judul sudah ada",TRUE);
define("sukses","<b>Alert!</b> Email berhasil dikirim.",TRUE);
define("gagal","<b>Alert!</b> Email gagal dikirim.",TRUE);
/************************************
*			REWGISTER ALERT			*Ukuran file melebihi batas maximum
************************************/
define("REGISTER","<b>Alert!</b> Anda berhasil mendaftar. cek email untuk aktivasi",TRUE);
/************************************
*			WARNA			*
************************************/
define("red","red",TRUE);
define("pink","pink",TRUE);
define("purple","purple",TRUE);
define("dp","deep-purple",TRUE);
define("indigo","indigo",TRUE);
define("blue","blue",TRUE);
define("lb","light-blue",TRUE);
define("cyan","cyan",TRUE);
define("teal","teal",TRUE);
define("green","green",TRUE);
define("lg","light green",TRUE);
define("lime","lime",TRUE);
define("yellow","yellow",TRUE);
define("amber","amber",TRUE);
define("orange","orange",TRUE);
define("do","deep-orange",TRUE);
define("brown","brown",TRUE);
define("grey","grey",TRUE);
define("bg","blue-grey",TRUE);
define("black","black",TRUE);










