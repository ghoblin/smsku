<?php
 // Define relative path from this script to mPDF
$judul='SMS_TERKIRIM_TGL_'.$_POST['dari'].'_S.D_'.$_POST['sampai']; //Beri nama file PDF hasil.
define('_MPDF_PATH','../MPDF60/');
include(_MPDF_PATH . "mpdf.php");

//Beginning Buffer to save PHP variables and HTML tags
ob_start();
	require_once('../g-asset/conn_db.php');
	require_once('../g-asset/web_function.php');
	require_once('../g-asset/functions.php');
	$dari = sanitize($_POST['dari']);
	$sampai = sanitize($_POST['sampai']);
?>

<h2>SMS TERKIRIM DARI <?=dtimes($dari,false,false);?> S.D <?=dtimes($sampai,false,false);?></h2>
                            <table style="width:100%" class="bpmTopnTail">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nomor hp</th>
                                                <th>Isi SMS</th>
                                                <th>Tanggal</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$sql = "SELECT * FROM sentitems  WHERE SendingDateTime between '$dari' AND '$sampai' ORDER BY SendingDateTime ASC";
$res = $mysqli->query($sql);
$no=1;
while($row = $res->fetch_array()){
	if($row['Status']=='SendingOKNoReport'){
	$status= 'TERKIRIM';
	}else{
	$status= 'GAGAL';
	}
	echo "<tr>";
	echo "<td>".$no++."</td>";
	echo "<td>".$row['DestinationNumber']."</td>";
	echo "<td>".$row['TextDecoded']."</td>";
	echo "<td>".dtimes($row['SendingDateTime'],true,false)."</td>";
	echo "<td>".$status."</td>";
	echo "</tr>";
}
	
?>
										</tbody>
                            </table>
<?php
$mpdf=new mPDF('utf-8', 'A4'); // Create new mPDF Document
 // $mpdf=new mPDF('c'); 
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
$stylesheet = file_get_contents('../css/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($judul.".pdf" ,'I');
exit;
?>