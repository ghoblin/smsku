<?php
	require_once('../g-asset/conn_db.php');
	require_once('../g-asset/library_function.php');
 // Define relative path from this script to mPDF
$judul='Phone book'.$tgl_sekarang; //Beri nama file PDF hasil.
define('_MPDF_PATH','../MPDF60/');
include(_MPDF_PATH . "mpdf.php");

//Beginning Buffer to save PHP variables and HTML tags
ob_start();

?>

<h2>PRINT SMS MASUK TANGGAL <?=$date_time;?></h2>
                            <table style="width:100%" class="bpmTopnTail">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nomor Pengirim</th>
                                                <th>Isi SMS</th>
                                                <th>Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$sql = "select * from inbox WHERE ReceivingDateTime >= CURDATE()  order by ID";
$res = $mysqli->query($sql);
$no=1;
while($row = $res->fetch_array()){
	echo "<tr>";
	echo "<td>".$no++."</td>";
	echo "<td>".$row['SenderNumber']."</td>";
	echo "<td>".$row['TextDecoded']."</td>";
	echo "<td>".$row['ReceivingDateTime']."</td>";
	echo "</tr>";
}
	
?>
										</tbody>
                            </table>
<?php
$mpdf=new mPDF('utf-8', 'A4'); // Create new mPDF Document
 // $mpdf=new mPDF('c'); 
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
$stylesheet = file_get_contents('../css/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($judul.".pdf" ,'I');
exit;
?>