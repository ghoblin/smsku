<!-- For Material Design Colors -->
<div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="defaultModalLabel">Kirim SMS</h4>
			</div>
			<div class="modal-body">
	<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="body">
  <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#flash" data-toggle="tab">FLASH SMS</a></li>
                                <li role="presentation"><a href="#groupsms" data-toggle="tab">GROUP SMS</a></li>
                                <li role="presentation"><a href="#schedule" data-toggle="tab">SCHEDULE SMS</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="flash">
	<form method="post" id="insert_form">  
<div class="demo-masked-input">
					<div class="row clearfix">
						<div class="col-md-12">
							<b>Nomor HP</b> <span class="error-no" style="display:none;color:#ff0000;"></span>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">phone_iphone</i>
								</span>
								
					<div class="search-box">
					<input type="text" id="nomorhpp" name="nomorhpp" class="form-control clearable" autocomplete="off" placeholder="Cari Nama, Nomor HP dengan +..." autofocus />
					<div class="result"></div>
					</div>
							</div>
						<b>Template SMS</b>
						<div class="input-group">
						<span class="input-group-addon">
									<i class="material-icons">settings_ethernet</i>
						</span>
						<div class="form-line">
                       <select name="mySelect" id="mySelect" onchange="myFunction()" class="form-control">
                          <option value="">-- Pilih Template --</option>
                    <?php
					$sqt = "SELECT judul,isi from template_sms";
					$rst = $mysqli->query($sqt);
                    while ($at = $rst->fetch_array()){
                            echo "<option value='$at[isi]'>$at[judul]</option>";
                    }
                    ?>
                        </select>
						</div>
						</div>
					<h2 class="card-inside-title">Pesan <span class="error" style="display:none;color:#ff0000;"></span></h2>
					<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">message</i>
								</span>
					<div class="form-line">
						<textarea rows="1" id="pesan" name="pesan" class="form-control no-resize auto-growth" placeholder="Tulis pesan" maxlength="153" onkeyup="countChar(this)"></textarea>
					</div>
					<div id="charNum"></div>
					<span class="success" style="display:none">Pesan terkirim</span>
					</div>
						</div>

					</div>
				</div>
			<div class="modal-footer">
				<button type="submit" name="insert" value="Insert" id="insert" class="btn btn-link bg-green waves-effect">KIRIM</button>
				<button type="button" class="btn btn-link bg-red waves-effect" data-dismiss="modal">TUTUP</button>
			</div>
	</form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="groupsms">
			<form id="group_sms" action="json/proses.php" method="POST">
			<div class="body">
				<div class="demo-masked-input">
					<div class="row clearfix">
						<div class="col-md-12">
				<label for="">Group Kontak <span class="errorg" style="display:none;color:#ff0000;"></span></label>
				<div class="input-group">
				<span class="input-group-addon">
									<i class="material-icons">group_work</i>
								</span>
					<div class="form-line">
                <select name="grups" value="" id="grups" class="form-control show-tick" required>
                    <option value="">-- Pilih Group --</option>
                    <?php
					$sq = "select * from pbk_groups";
					$rs = $mysqli->query($sq);
                    while ($a = $rs->fetch_array()){
                            echo "<option value='$a[1]'>$a[0]</option>";
                    }
                    ?>
                </select>
					</div>
				</div>
						<b>Template SMS</b>
						<div class="input-group">
						<span class="input-group-addon">
									<i class="material-icons">settings_ethernet</i>
						</span>
						<div class="form-line">
                       <select name="mySelectg" id="mySelectg" onchange="myFunctiong()" class="form-control">
                          <option value="">-- Pilih Template --</option>
                    <?php
					$sqt = "SELECT judul,isi from template_sms";
					$rst = $mysqli->query($sqt);
                    while ($at = $rst->fetch_array()){
                            echo "<option value='$at[isi]'>$at[judul]</option>";
                    }
                    ?>
                        </select>
						</div>
						</div>
					<label for="">Pesan <span class="error" style="display:none;color:#ff0000;"></span></label>
					<div class="input-group">
								<span class="input-group-addon">
									<i class="material-icons">message</i>
								</span>
					<div class="form-line">
						<textarea rows="2" id="pesans" name="pesans" class="form-control no-resize auto-growth" placeholder="Tulis pesan" maxlength="160" onkeyup="countChar(this)" required></textarea>
					</div>
					<div id="charNum"></div>
				</div>
						</div>

					</div>
				</div>
			</div>
                <div class="modal-footer">
                    <button type="button" id="submitForm" class="btn btn-link btn-success waves-effect">KIRIM</button>
                    <button type="button" class="btn btn-link  bg-red" data-dismiss="modal">TUTUP</button>
                </div>
			</form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="schedule">
                                    <b>Message Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                        pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                        sadipscing mel.
                                    </p>
                                </div>
                            </div><!--e:tab-content-->
			</div><!--e:body-->
		</div><!--e:card-->
	</div><!--e:col-->
</div><!--e:row-->

			</div><!--e:modal-body-->
		</div><!--e:modal-content-->
	</div><!--e:modal-dialog-->
</div><!--e:modal-->
<!-- the div that represents the modal dialog -->
<!-- profile modal -->
<div class="modal fade" id="EditProfile" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Profile</h4>
            </div>
            <div class="modal-body">
                <div id="loadprof"></div>
                <div class="edit-profile"></div>
            </div>
        </div>
    </div>
</div>
<!-- info modal -->
<div class="modal fade" id="EditInfo" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Info</h4>
            </div>
            <div class="modal-body">
                <div id="loadinfo"></div>
                <div class="edit-info"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="dialog-modem" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabeli">Modem</h4>
                        </div>
                        <div class="modal-body">
						<div id='ajax_loader2' style="position: fixed; left: 50%; top: 50%; display: none;">
						<img src="images/loading.gif">
						</div>
						<div class="modal-run"></div>
                        </div>
						<div class="modal-footer">
							<button type="button" class="btn bg-red btn-link waves-effect" data-dismiss="modal">CLOSE</button>
						</div>
                    </div>
                </div>

            </div>
			<div class="modal fade" id="play-koneksi" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabelplay">Modem</h4>
                        </div>
                        <div class="modal-body">
						<div id='ajax_loader3' style="position: fixed; left: 50%; top: 50%; display: none;">
						<img src="images/loading.gif">
						</div>
						<div class="play-koneksi"></div>
                        </div>
						<div class="modal-footer">
							<button type="button" class="btn bg-red  btn-link waves-effect" data-dismiss="modal">CLOSE</button>
						</div>
                    </div>
                </div>
            </div>
				<div class="modal fade" id="stop-koneksi" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabelstop">Modem</h4>
                        </div>
                        <div class="modal-body">
						<div id='ajax_loader_stop' style="position: fixed; left: 50%; top: 50%; display: none;">
						<img src="images/loading.gif">
						</div>
						<div class="stop-koneksi"></div>
                        </div>
						<div class="modal-footer">
							<button type="button" class="btn bg-red btn-link waves-effect" data-dismiss="modal">CLOSE</button>
						</div>
                    </div>
                </div>
            </div>
<!-- baca modal -->
<div class="modal fade" id="BacaModalc" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Baca Pesan</h4>
            </div>
            <div class="modal-body">
			<div id="loadin"></div>
                <div class="baca-datain"></div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-link waves-effect" data-dismiss="modal">TUTUP</button>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="cek-koneksi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabelcek">Modem</h4>
			</div>
			<div class="modal-body">
			<div id='ajax_loader' style="position: fixed; left: 50%; top: 50%; display: none;">
			<img src="images/loading.gif">
			</div>
			<div class="pbcek"></div>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="backup" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabelb">Modem</h4>
			</div>
			<div class="modal-body">
			<div id='ajax_loaderb' style="position: fixed; left: 50%; top: 50%; display: none;">
			<img src="images/loading.gif">
			</div>
			<div class="backupdb"></div>
			</div>
			<div class="modal-footer">
			<div style="float:left">by <a href='http://rangkasku.web.id/' target='_blank'>rangkasku.web.id</a></div>
				<button type="button" class="btn btn-danger btn-link waves-effect" data-dismiss="modal">TUTUP</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="kirim-email" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabelm">Kirim Email</h4>
			</div>
			<div class="modal-body">
			<form method="post" id="insert_email"  enctype="multipart/form-data">
			<div class="box-body">
			<div class="alert alert-danger" role="alert" style="display:none;" id="removeWarning">
			</div>
				<div class="form-group form-float">
					<label  class="form-label">To</label>
					<select placeholder="Pilih Email" name="email[]" class="selectize-control" id="email" multiple required>
					<?php echo emailtg();?>
					</select>
				</div>
				<div class="form-group form-float">
					<label  class="form-label">Dari</label>
					<div class="form-line">
					<input type="text" name="txtFormName" id="txtFormName" class="form-control" value="SMS Gateway Localhost Serang" required>
					</div>
				</div>
				<div class="form-group hidden">
					<label  class="form-label">Pengirim </label>
					<input type="hidden" name="txtFormEmail" id="txtFormEmail" value="no-replay@rangkasku.web.id" class="form-control">
				</div>
				<div class="form-group form-float">
					<label  class="form-label">Judul</label>
					<div class="form-line">
					<input type="text" name="txtSubject" id="txtSubject" class="form-control" required>
					</div>
				</div>
				<div class="form-group form-float">
					<label  class="form-label">Isi Pesan</label>
					<div class="form-line">
					<textarea cols="2" class="form-control no-resize auto-growth"  name="txtDescription" id="txtDescription" required></textarea>
					</div>
				</div>
				<div class="form-group">
					<input type='file' name='fileAttach' class="form-control">
				</div>
				</div>
			<div class="modal-footer">
				<button type="submit" id="insertMail"  name="submit" class="btn bg-cyan btn-link waves-effect">Kirim</button>
				<button type="button" class="btn bg-red btn-link waves-effect" data-dismiss="modal">TUTUP</button>
			</div>
			</form>
			</div><!-- /.modal-body -->
			</div>
		</div>
	</div>
<script>
$('.modal').on('hidden.bs.modal', function(){
	$(this).removeData('bs.modal');
});
</script>