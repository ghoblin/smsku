<?php

//fungsi filter 1
function antiinjection($data){
  $filter_sql = mysqli_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
  return $filter_sql;
}
function filter($mysqli,$data) {
	// global $mysqli;
    $data = trim(htmlentities(strip_tags($data)));
 
    if (get_magic_quotes_gpc())
        $data = stripslashes($data);
 
    $data = mysqli_real_escape_string($mysqli,$data);
 
    return $data;
}
function safe($value)
{
   global $mysqli;

   $secureString = mysqli_real_escape_string($mysqli, $value);

   return $secureString;
}
?>
