<?php
/**
 * @return IP (192.168.1.1)
 */
function ip_user()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    	$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;
}

/**
 * @see http://php.net/manual/en/function.get-browser.php;
 * @return 
 */
function browser_user()
{
	$browser = _userAgent();
	return $browser['name'] . ' v.'.$browser['version'];
}
# User Agent
function _userAgent()
{
	$u_agent 	= $_SERVER['HTTP_USER_AGENT']; 
    $bname   	= 'Unknown';
    $platform 	= 'Unknown';
    $version 	= "";

	$os_array       =   array(
	                        '/windows nt 6.2/i'     =>  'Windows 8',
	                        '/windows nt 6.1/i'     =>  'Windows 7',
	                        '/windows nt 6.0/i'     =>  'Windows Vista',
	                        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
	                        '/windows nt 5.1/i'     =>  'Windows XP',
	                        '/windows xp/i'         =>  'Windows XP',
	                        '/windows nt 5.0/i'     =>  'Windows 2000',
	                        '/windows me/i'         =>  'Windows ME',
	                        '/win98/i'              =>  'Windows 98',
	                        '/win95/i'              =>  'Windows 95',
	                        '/win16/i'              =>  'Windows 3.11',
	                        '/macintosh|mac os x/i' =>  'Mac OS X',
	                        '/mac_powerpc/i'        =>  'Mac OS 9',
	                        '/linux/i'              =>  'Linux',
	                        '/ubuntu/i'             =>  'Ubuntu',
	                        '/iphone/i'             =>  'iPhone',
	                        '/ipod/i'               =>  'iPod',
	                        '/ipad/i'               =>  'iPad',
	                        '/android/i'            =>  'Android',
	                        '/blackberry/i'         =>  'BlackBerry',
	                        '/webos/i'              =>  'Mobile'
	                    );

	foreach ($os_array as $regex => $value) { 

	    if (preg_match($regex, $u_agent)) {
	        $platform    =   $value;
	    }

	}
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
   
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'   => $pattern
    );
}

/**
 * @return name Operating System*/
function os_user()
{
	$OS = _userAgent();
	return $OS['platform'];
}
function info($table,$array,$id)
{
global $mysqli;
$sqli = $mysqli->query("SELECT * FROM ".$table." where id_".$table."='".$id."'");
$datai=$sqli->fetch_array();
echo $datai[$array];
return $datai;
}
function user($array)
{
global $mysqli;
$sqli = $mysqli->query("SELECT * FROM user where username='".$_SESSION['namauser']."'");
$datai=$sqli->fetch_array();
echo $datai[$array];
return $datai;
}
function sesi($sesi)
{
$sesi =	$_SESSION[$sesi];
return $sesi;
}
//penambahan fungsi nomor
function nomor($num1,$num2,$nama,$number)
{
		if($num1 == $num2){
		$sender = $nama;
		}else{
		$sender = $number;
		}
		return $sender;
}
//penambahan fungsi nomorb 
function nomorb($num1,$num2,$nama,$number)
{
		if($num1 == $num2){
		$sender = "<b>".$nama."</b>";
		}else{
		$sender = "<b>".$number."</b>";
		}
		return $sender;
}
// Returns null if unable to determine drive letter (such as on a *nix box)
function driveLetter($path)
{
    return (preg_match('/^[A-Z]:/i', $path = realpath($path))) ? $path[0] : null;
}
function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }
function sanitize($input) {
	global $mysqli;
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = mysqli_real_escape_string($mysqli,$input);
    }
    return $output;
}

function is_connected()
{
    $connected = @fsockopen("www.kalkulatorcetak.com", 80); 
                                        //website, port  (try 80 or 443)
    if ($connected){
        $is_conn = true; //action when connected
        fclose($connected);
    }else{
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}
  function parse($no){
	$data = substr($no,0,9);
	return $data;
  }
  function parse2($no){
	$data = substr($no,3,9);
	return $data;
  }
function check_internet_connection($sCheckHost = 'www.smsku.cu.cc')
{
    return (bool) @fsockopen($sCheckHost, 80, $iErrno, $sErrStr, 5);
}
?>