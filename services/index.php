<?php
session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
echo json_encode(array(404 => "error"));
}else{
include __DIR__ . '/../g-asset/conn_db.php';
	include __DIR__ . '/../g-asset/functions.php';
$step = (isset($_GET['step']) ? $_GET['step'] : '');
$sqlu = $mysqli->query("SELECT * FROM  user WHERE username='$_SESSION[namauser]'");
$datau=$sqlu->fetch_array();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | <?php info('info',1,2);?></title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="../css/roboto-font.css" rel="stylesheet" type="text/css">
    <!-- Material icon Css -->
    <link href="../css/material-icon.css" rel="stylesheet">
    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">
 
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
	<link href="../plugins/notify/styles/metro/notify-metro.css" rel="stylesheet" />
</head>
<body class="theme-red">
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="../read.php?module=home">SMS Gateway</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
   <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
				<div class="image">
                    <img src="../images/munajat.jpg" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo sesi('namalengkap');?></div>
                    <div class="email"><?php echo sesi('mailuser');?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="">
                        <a href="index.php?module=home">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
					<li class="header">FOLDER</li>
                    <li>
                        <a href="../read.php?module=inbox">
                            <i class="material-icons">inbox</i>
                            <span>Kotak Masuk</span>
                        </a>
                    </li>
					<li>
                        <a href="../read.php?module=outbox">
                            <i class="material-icons">move_to_inbox</i>
                            <span>Kotak Keluar</span>
                        </a>
                    </li>
					<li>
                        <a href="../read.php?module=send">
                            <i class="material-icons">send</i>
                            <span>Pesan Terkirim</span>
                        </a>
                    </li>
					<li>
                        <a href="../read.php?module=cast">
                            <i class="material-icons">cast</i>
                            <span>Pesan Siaran</span>
                        </a>
                    </li>
					<li>
                        <a href="../read.php?module=schedule">
                            <i class="material-icons">schedule</i>
                            <span>Pesan Terjadwal</span>
                        </a>
                    </li>
					 <li class="header">SETTINGS</li>
                    <li>
                        <a href="../read.php?module=changelogs">
                            <i class="material-icons">update</i>
                            <span>Changelogs</span>
                        </a>
                    </li>
					<li>
                        <a href="../logout.php">
                            <i class="material-icons">power_settings_new</i>
                            <span>Keluar</span>
                        </a>
                    </li>
					
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
<?php 
$sqlv = $mysqli->query("SELECT * FROM changelogs order by id desc limit 1");
$datav=$sqlv->fetch_array();
$copyYear = 2016; 
$curYear = date('Y'); 
echo '&#169;&#160;' . $copyYear . (($copyYear != $curYear) ? ' - ' . $curYear : ''); 
?> <a href="javascript:void(0);"><?php info('info',1,2);?></a> <b>Versi: </b> <a href="?module=changelogs"><?=$datav['judul'];?></a>
                </div>
                <div class="version">
                   Style by <a href="https://gurayyarar.github.io/AdminBSBMaterialDesign/" target="_blank">AdminBSB</a> <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->

    </section>
    <!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>
    <script src="../js/admin.js"></script>
	<script src="../plugins/notify/notify.js"></script>
	<script src="../plugins/notify/styles/metro/notify-metro.js"></script>
	<script src="../js/pages/ui/tooltips-popovers.js"></script>	
<script type="text/javascript">  
    function delnotif(style,msg) {
        $.notify({
            title: 'Notification Delete',
            text: msg,
            image: "<i class='material-icons col-red'>delete</i>"
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
    }
// delnotif('white');
// window.location.reload(1);
</script>
<section class="content">
<?php 
$pathFile = "step.php";
include __DIR__ . '/'.$pathFile;
?>
 </section>

<script type="text/javascript">

$(document).ready(function(){
		var id_cek = 0;
        $('.cek').on("click", function(){
			 $('#ajax_loader').show();
			  $(".pbcek").html('').hide();
            var url = "cek-koneksi.php";
            id_cek = this.id;
            if(id_cek != 0) {
                $("#myModalLabelcek").html("Cek Koneksi Modem");
            } else {
                $("#myModalLabelcek").html("Tambah Modem");
            }
            $.post(url, {id: id_cek} ,function(data) {
                $(".pbcek").html(data).show();
				$('#ajax_loader').hide();
            });
			
        });
		var id_edit = 0;
        $('.edit').on("click", function(){
			 $('#ajax_loader').show();
			 $(".pbedit").html('').hide();
            var url = "edit-modem.php";
            id_cek = this.id;
            if(id_cek != 0) {
                $("#myModalLabeledit").html("Edit FILE");
            } else {
                $("#myModalLabeledit").html("Tambah Modem");
            }
            $.post(url, {id: id_cek} ,function(data) {
                $(".pbedit").html(data).show();
				$('#ajax_loader').hide();
            });
			
        });
		var id_play = 0;
        $('.plays').on("click", function(){
			 $('#ajax_loader3').show();
			  $(".play-koneksi").html('').show();
            var url = "play-koneksi.php";
            id_play = this.id;
            if(id_play != 0) {
                $("#myModalLabelplay").html("Install & Started Service GammuSMSD");
            } else {
                $("#myModalLabelplay").html("error");
            }
            $.post(url, {id: id_play} ,function(data) {
                $(".play-koneksi").html(data).show();
				$('#ajax_loader3').hide();
            });
			
        });
		var id_stop = 0;
        $('.stop').on("click", function(){
			 $(".stop-koneksi").html('').show();
			 $('#ajax_loader_stop').show();
            var url = "stop-koneksi.php";
            id_stop = this.id;
            if(id_stop != 0) {
                $("#myModalLabelstop").html("Stop Service Modem");
            } else {
                $("#myModalLabelstop").html("error");
            }
            $.post(url, {id: id_stop} ,function(data) {
                $(".stop-koneksi").html(data).show();
				$('#ajax_loader_stop').hide();
            });
			
        });
		var id_kode = 0;
        $('.kode').on("click", function(){
			 $('#ajax_loader_kode').show();
			  $(".cek-sms").html('').show();
            var url = "cek_pulsa.php";
            id_kode = this.id;
            if(id_kode != 0) {
                $("#myModalLabelkode").html("Cek PULSA");
            } else {
                $("#myModalLabelkode").html("error");
            }
            $.post(url, {id: id_kode} ,function(data) {
                $(".cek-sms").html(data).show();
				$('#ajax_loader_kode').hide();
            });
			
        });
$('#confirm-delete').on('show.bs.modal', function(e) {
	$(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
	
	$('.debug-url').html('Delete URL: <strong>' + $(this).find('.danger').attr('href') + '</strong>');
})

});

</script>

</body>
</html>
<?php } ?>