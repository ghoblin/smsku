<div class="steps clearfix">
<ul role="tablist">
<li role="tab" class="first done" aria-disabled="false" aria-selected="false">
<a id="wizard_horizontal-t-0" href="?step=1#wizard_horizontal-h-0" aria-controls="wizard_horizontal-p-0"><span class="number">1.</span> Setting Modem</a>
</li>
<li role="tab" class="done" aria-disabled="false" aria-selected="false">
<a id="wizard_horizontal-t-1" href="?step=2#wizard_horizontal-h-1" aria-controls="wizard_horizontal-p-1"><span class="number">2.</span> Status Modem</a>
</li>
<li role="tab" class="current" aria-disabled="false" aria-selected="true"><a id="wizard_horizontal-t-2" href="#wizard_horizontal-h-2" aria-controls="wizard_horizontal-p-2"><span class="current-info audible">current step: </span><span class="number">3.</span> Kirim SMS</a>
</li>
<li role="tab" class="disabled last" aria-disabled="true">
<a id="wizard_horizontal-t-3" href="#wizard_horizontal-h-3" aria-controls="wizard_horizontal-p-3"><span class="number">4.</span> Cek SMS Masuk</a>
</li>
</ul>
</div>
<div class="content clearfix">
<h2 id="wizard_horizontal-h-0" tabindex="-1" class="title">First Step</h2>
<section id="wizard_horizontal-p-0" role="tabpanel" aria-labelledby="wizard_horizontal-h-0" class="body" aria-hidden="true" style="left: -927px; display: none;">
<p></p>
</section>

<h2 id="wizard_horizontal-h-1" tabindex="-1" class="title">Second Step</h2>
<section id="wizard_horizontal-p-1" role="tabpanel" aria-labelledby="wizard_horizontal-h-1" class="body" style="display: none; left: -927px;" aria-hidden="true">
<p></p>
</section>

<h2 id="wizard_horizontal-h-2" tabindex="-1" class="title current">Third Step</h2>
<section id="wizard_horizontal-p-2" role="tabpanel" aria-labelledby="wizard_horizontal-h-2" class="body current" style="display: block; left: 0px;" aria-hidden="false">
								
	<form method="post" action="?step=3&op=send">
		<div class="row clearfix">
			<div class="col-md-3 form-control-label">
				<label for="">Nomor Tujuan</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control" name="nohp" placeholder="Nomor Tujuan" value="" required>
					</div>
				</div>
			</div>
			<div class="col-md-3 form-control-label">
				<label for="">Modem</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
		<select name="modem" class="form-control">
		<?php
			$query = "SELECT ID FROM phones ORDER BY ID";
			$rs = $mysqli->query($query);
			while ($data = $rs->fetch_array())
			{
				echo "<option>".$data['ID']."</option>";
			}
		?>
		</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 form-control-label">
				<label for="">Pesan SMS (Max. 160 karakter)</label>
			</div>
			<div class="col-md-9">
				<div class="form-group">
					<div class="form-line">
						<textarea name="pesan" class="form-control" rows="1" required></textarea>
					</div>
				</div>
			<input type="submit" name="submit" class="btn btn-link bg-green waves-effect"  value="Kirim SMS">
			</div>
		</div>
		
	</form>
	<?php
	if (isset($_GET['op']))
	{
		if ($_GET['op'] == 'send')
		{
			$nohp = $_POST['nohp'];
			$modem = $_POST['modem'];
			$pesan = $_POST['pesan'];
$sql2 = "INSERT INTO outbox (DestinationNumber, TextDecoded, SenderID, CreatorID) VALUES ('$nohp', '$pesan','$modem', 'Gammu')";

if ($mysqli->query($sql2) === TRUE) {
echo "<span class='label label-info' >SMS dalam proses pengiriman</span>";
} else {
echo "Pengiriman SMS gagal: " . $sql2 . "<br>" . $mysqli->error."'";
}
		}
	}
	?>
                                </section>
                            </div>