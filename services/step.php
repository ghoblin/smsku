﻿ <div class="container-fluid">
            <!-- Basic Example | Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>SETTING MODEM SMS GATEWAY</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
						
						<div id="wizard_horizontal" role="application" class="wizard clearfix">
						<?php 
						if($step=='home'){
							
						}elseif($step==1){
						include "step-1.php";	
						}elseif($step==2){
						include "step-2.php";	
						}elseif($step==3){
						include "step-3.php";	
						}elseif($step==4){
						include "step-4.php";	
						}
						if($step!=''){
						?>	
							<div class="actions clearfix">
							<ul role="menu" aria-label="Pagination">



<?php
    if($step==4)
    {
		$disabled = "disabled";
		$display = "block";
		$display1 = "none";
		$display2 = "";
	}else{
		$disabled = "";
		$display = "none";
		$display1 = "";
		$display2 = "";
		
	}
	 if($step==1)
	 {
		$display2 = "none";
		 
	 }
	$hitung = 4;
    $batas = 4;

    // Mengecek eksistensi variabel '?page'


	if(isset($_GET['step']))
    {
       $page = $_GET['step'] + 1;
       $offset = $batas * $page ;
    }
    else
    {
       $page = 0;
       $offset = 0;
    }
    if( $page > 0 )
    {
       $last = $page - 2;
	echo '
	<li class="" style="display: '.$display2.';" aria-disabled="false">
	<a href="?step='.$last.'" role="menuitem" class="waves-effect">Previous</a>
	</li>
	<li aria-hidden="false" style="display: '.$display1.';" class="'.$disabled.'" aria-disabled="false">
	<a href="?step='.$page.'" role="menuitem" class="waves-effect">Next</a>
	</li>
	<li style="display: '.$display.'" aria-hidden="false">
	<a href="../" role="menuitem" class="waves-effect">Finish</a>
	</li>
	';
       // echo "<a href=\"?module=service&step=$last\">Prev</a> |";
       // echo "<a href=\"?module=service&step=$page\">Next</a>";
    }
    else if( $page == 0 )
    {
       echo "<a href=\"?step=$page\">Next</a>";
    }
    else if( $left_rec < $batas )
    {
       $last = $page - 2;
       echo "<a href=\"?step=$last\">Prev</a>";
    }
?>
							</ul>
							</div>
						<?php }else{ ?>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                    <a href="?step=1" class="btn btn-primary btn-block waves-effect" data-toggle="tooltip" data-placement="top" title="SETTING MODEM"> <i class="material-icons">settings</i> SETTING MODEM</a>
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
								<a href="?step=2" class="btn btn-success btn-block waves-effect" data-toggle="tooltip" data-placement="top" title="CEK SERVICE MODEM"> <i class="material-icons">check</i> CEK SERVICE MODEM</a>
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                    <button type="button" class="btn btn-primary btn-block waves-effect" data-toggle="tooltip" data-placement="top" title="TEST KIRIM SMS"><i class="material-icons">send</i> TEST KIRIM SMS</button>
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                    <button type="button" class="btn btn-primary btn-block waves-effect" data-toggle="tooltip" data-placement="top" title="CEK SMS MASUK"><i class="material-icons">message</i> CEK SMS MASUK</button>
                                </div>
                            </div>
                        </div>
						<?php } ?>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Example | Horizontal Layout -->
        </div>
            <!-- For Material Design Colors -->
            <div class="modal fade" id="confirm" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content modal-col-red">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Confirm Delete</h4>
                        </div>
                        <div class="modal-body">
						<p>Anda yakin akan menghapus tautan ini, prosedur tidak dapat diubah.</p>
						<p>Apakah Anda ingin melanjutkan?</p>
                        </div>
                        <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-link btn-danger  waves-effect" id="delete">HAPUS</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                        </div>
                    </div>
                </div>
            </div>
