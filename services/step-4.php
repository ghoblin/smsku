<div class="steps clearfix">
<ul role="tablist">
<li role="tab" class="first done" aria-disabled="false" aria-selected="false"><a id="wizard_horizontal-t-0" href="?step=1" aria-controls="wizard_horizontal-p-0"><span class="number">1.</span> Setting Modem</a>
</li>
<li role="tab" class="done" aria-disabled="false" aria-selected="false"><a id="wizard_horizontal-t-1" href="?step=2" aria-controls="wizard_horizontal-p-1"><span class="number">2.</span>  Status Modem</a>
</li>
<li role="tab" class="done" aria-disabled="false" aria-selected="false"><a id="wizard_horizontal-t-2" href="?step=3" aria-controls="wizard_horizontal-p-2"><span class="number">3.</span> Kirim SMS</a>
</li>
<li role="tab" class="last current" aria-disabled="false" aria-selected="true"><a id="wizard_horizontal-t-3" href="#wizard_horizontal-h-3" aria-controls="wizard_horizontal-p-3"><span class="current-info audible">current step: </span><span class="number">4.</span> Cek SMS Masuk</a>
</li>
</ul>
</div>
<div class="content clearfix">

<h2 id="wizard_horizontal-h-3" tabindex="-1" class="title current">Forth Step</h2>
<section id="wizard_horizontal-p-3" role="tabpanel" aria-labelledby="wizard_horizontal-h-3" class="body current" style="display: block; left: 0px;" aria-hidden="false">
<script type="text/javascript">
window.onload = function(event) {
       ajaxrunning();
};
  </script>
	<script type="text/javascript">
			function ajaxrunning()
			{
				if (window.XMLHttpRequest)
				{
					xmlhttp=new XMLHttpRequest();
				}
				else
				{
					xmlhttp =new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("inbox").innerHTML = xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","run.php");
				xmlhttp.send();
				setTimeout("ajaxrunning()", 5000); 
			}
	</script>

	<div id="inbox"></div>
</section>
</div>
