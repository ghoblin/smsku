<div class="steps clearfix">
<ul role="tablist">
<li role="tab" class="first done" aria-disabled="false" aria-selected="false"><a id="wizard_horizontal-t-0" href="index.php?step=1" aria-controls="wizard_horizontal-p-0"><span class="number">1.</span> Setting Modem</a>
</li>

<li role="tab" class="current" aria-disabled="false" aria-selected="true"><a id="wizard_horizontal-t-1" href="#wizard_horizontal-h-1" aria-controls="wizard_horizontal-p-1"><span class="current-info audible">current step: </span><span class="number">2.</span>  Status Modem</a>
</li>
<li role="tab" class="disabled" aria-disabled="true">
<a id="wizard_horizontal-t-2" href="#wizard_horizontal-h-2" aria-controls="wizard_horizontal-p-2"><span class="number">3.</span> Kirim SMS</a>
</li>
<li role="tab" class="disabled last" aria-disabled="true">
<a id="wizard_horizontal-t-3" href="#wizard_horizontal-h-3" aria-controls="wizard_horizontal-p-3"><span class="number">4.</span> Cek SMS Masuk</a>
</li>
</ul>
</div>
<?php
// menjalankan command untuk mengenerate file service.log
passthru("net start > service.log");

// membuka file service.log
$handle = fopen("service.log", "r");

// status awal = 0 (dianggap servicenya tidak jalan)
$status = 0;

// proses pembacaan isi file
while (!feof($handle))
{
   // baca baris demi baris isi file
   $baristeks = fgets($handle);
   if (substr_count($baristeks, 'Gammu SMSD Service (GammuSMSD)') > 0)
   {
     // jika ditemukan baris yang berisi substring 'Gammu SMSD Service (GammuSMSD)'
     // statusnya berubah menjadi 1
     $status = 1;
   }
}
// menutup file
fclose($handle);
// jika status terakhir = 1, maka gammu service running
if ($status == 1){
$warna = "#00ff00";
$online = "MODEM ONLINE";
$btn = '<button type="button" data-toggle="tooltip" data-placement="top" title="Modem Aktif" class="btn bg-green btn-circle-lg waves-effect waves-circle waves-float"><i class="material-icons">play_circle_filled</i></button>';
}
// jika status terakhir = 0, maka service gammu berhenti
elseif ($status == 0) {
$warna = "#ff0000";
$online = "MODEM OFF LINE";
$btn = '<button type="button" data-toggle="tooltip" data-placement="top" title="Modem Tidak Aktif" class="btn bg-red btn-circle-lg waves-effect waves-circle waves-float"><i class="material-icons">stop</i></button>';
}
?>
<div class="content clearfix" style="background:<?=$warna;?>">
<h2 id="wizard_horizontal-h-0" tabindex="-1" class="title">First Step</h2>
<section id="wizard_horizontal-p-0" role="tabpanel" aria-labelledby="wizard_horizontal-h-0" class="body" aria-hidden="true" style="left: -927px; display: none;">
	<p>

	</p>
</section>
<h2 id="wizard_horizontal-h-1" tabindex="-1" class="title current">Second Step</h2>
<section id="wizard_horizontal-p-1" role="tabpanel" aria-labelledby="wizard_horizontal-h-1" class="body current" style="display: block; left: 0px;" aria-hidden="false">
<div class="icon-button-demo">
<h2 style="color:#fff;text-align:center;padding-top:7%"><?=$btn;?></h2>
</div>
</section>
</div>